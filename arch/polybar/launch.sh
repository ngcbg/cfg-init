#!/usr/bin/env bash

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

# Launch example
polybar topbar &
#echo "---" | tee -a /tmp/polybar1.log

#polybar -c .config/polybar/config bottombar & >>/tmp/polybar1.log 2>&1 & disown


echo "Bars launched..."
