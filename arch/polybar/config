
[colors]
background = #2c415a
background-alt = #444
foreground = #e4e4e4
foreground-alt = #555
primary = #ffb52a
secondary = #e60053
alert = #bd2c40

[bar/bottombar]
monitor = ${env:MONITOR:HDMI-0}
width = 100%
height = 30
bottom = true
radius = 6.0
fixed-center = false

background = ${colors.background}
foreground = ${colors.foreground}

line-size = 2
line-color = #f00

border-size = 4
border-color = #00000000

padding-left = 1
padding-right = 1

module-margin-left = 2
module-margin-right = 2

font-0 = "fixed:pixelsize=11;2"
font-1 = "Feather:size=12;2"
font-2 = "xos4 Terminus:size=11;1"
font-3 = "Fantasque Sans Mono:style:regular:pixelsize=10;1"
font-4 = "icomoon:pixelsize=12;3""
font-5 = "Symbols Nerd Font:style:1000-em:pixelsize=12;3"
font-6 = "Font Awesome:style:regular:pixelsize=10;2"
font-7 = "DejaVu Sans:size:12;1"

modules-left = menu xworkspaces cpu temperature1 memory
modules-center = xwindow filesystem
modules-right = eth xkeyboard date powermenu

tray-position = right
tray-padding = 2
tray-scale = 1

cursor-click = pointer
cursor-scroll = ns-resize

[module/xwindow]
type = internal/xwindow
label =   %title:0:30:...%

[module/xworkspaces]
type = internal/xworkspaces
pin-workspaces = true
enable-click = true
enable-scroll = false
;format-padding = 1
icon-0 = ▰▱▱▱
icon-1 = ▱▰▱▱
icon-2 = ▱▱▰▱
icon-3 = ▱▱▱▰
;icon-4 = V
;icon-5 = VI
;icon-6 = VII
;icon-7 = 8;
;icon-6 = 8;
;icon-9 = 10;
;icon-default = ♟
format = <label-state>
format-underline = #ffb52a

[module/xkeyboard]
type = internal/xkeyboard
blacklist-0 = num lock
format-prefix = "  "
format-prefix-foreground = ${colors.foreground}
label-layout = %layout%
label-indicator-padding = 1
label-indicator-margin = 1
label-indicator-background = ${colors.secondary}
label-indicator-underline = ${colors.secondary}
format-underline = #ffb52a

[module/filesystem]
type = internal/fs
interval = 25
mount-0 = /
label-mounted =  %percentage_used%%
label-unmounted = %mountpoint% not mounted
label-unmounted-foreground = ${colors.foreground-alt}

[module/cpu]
type = internal/cpu
interval = 2
format-padding = 1
format-prefix = " "
format-prefix-foreground = ${colors.foreground}
#format-underline = #f90000
label = %percentage:2%%

[module/memory]
type = internal/memory
format-padding = 1
interval = 3
;format-prefix = "  "
format-prefix = "  "
format-prefix-foreground = ${colors.foreground}
label = %percentage_used%% %mb_used%

[module/eth]
type = internal/network
interface = enp8s0
interval = 10
format-padding = 1
#format-connected-underline = #55aa55
format-connected-prefix = " "
format-connected-prefix-foreground = ${colors.foreground}
label-connected = %local_ip%
format-disconnected =

[module/date]
type = internal/date
interval = 5
date =
date-alt = " %Y-%m-%d"
time = %H:%M
time-alt = %H:%M:%S
format-prefix = 
format-prefix-foreground = ${colors.foreground}
label = %date% %time%
format-underline = #ffb52a

[module/pulseaudio]
type = internal/pulseaudio
sink = alsa_output.pci-0000_12_00.3.analog-stereo
use-ui-max = true
interval = 5
format-volume = <ramp-volume> <label-volume>
label-muted =  Muted
label-muted-foreground = ${colors.foreground}
ramp-volume-0 = 
ramp-volume-1 = 
ramp-volume-2 = 
ramp-volume-3 = 
ramp-volume-4 = 
format-underline = #ffb52a

[module/temperature1]
type = custom/script
exec = ~/.bin/cpu_temp.sh
interval = 5
format-padding = 1
format-foreground = ${colors.foreground}
format-background = ${colors.background}
format-prefix-foreground = #C1B93E
label = %output:0:150:%

[module/alsa]
type = internal/alsa
format-volume = <label-volume> <bar-volume>
label-volume = VOL
label-volume-foreground = ${root.foreground}
format-muted-prefix = " "
format-muted-foreground = ${colors.foreground-alt}
label-muted = sound muted
bar-volume-width = 10
bar-volume-foreground-0 = #55aa55
bar-volume-foreground-1 = #55aa55
bar-volume-foreground-2 = #55aa55
bar-volume-foreground-3 = #55aa55
bar-volume-foreground-4 = #55aa55
bar-volume-foreground-5 = #f5a70a
bar-volume-foreground-6 = #ff5555
bar-volume-gradient = false
bar-volume-indicator = |
bar-volume-indicator-font = 2
bar-volume-fill = ─
bar-volume-fill-font = 2
bar-volume-empty = ─
bar-volume-empty-font = 2
bar-volume-empty-foreground = ${colors.foreground-alt}

[module/powermenu]
type = custom/menu
expand-right = true
format-spacing = 1
format-padding = 1
label-open = 
label-open-foreground = ${colors.foreground}
label-close = 
label-close-foreground = ${colors.secondary}
label-separator = |
label-separator-foreground = ${colors.foreground-alt}
menu-0-0 = 
menu-0-0-exec = reboot
menu-0-1 = 
menu-0-1-exec = poweroff
menu-0-2 = 
menu-0-2-exec = openbox --restart
format-underline = #ffb52a

[module/menu]
type = custom/script
exec = ~/.config/polybar/scripts/menu
click-left = ~/.config/polybar/scripts/launcher &
format-spacing = 1
format-underline = 1
format-padding = 2

[global/wm]
margin-top = 0
margin-bottom = 0

[settings]
;compositing-background = over
;compositing-background = screen
;compositing-foreground = source
;compositing-border = over
;pseudo-transparency = false
throttle-output = 5
throttle-output-for = 10
throttle-input-for = 30
screenchange-reload = true
compositing-background = over
compositing-foreground = over
compositing-overline = over
compositing-underline = over
compositing-border = over

; vim:ft=dosini
