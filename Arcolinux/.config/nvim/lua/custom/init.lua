vim.api.nvim_create_autocmd({ "VimEnter" }, {
  pattern = { "*" },
  command = "highlight link CursorLine CursorColumn",
})

-- vim.cmd('autocmd VimEnter * PlugInstall --sync ')

local vim = vim
local Plug = vim.fn['plug#']

vim.call('plug#begin')

Plug('luukvbaal/nnn.nvim')
Plug ('dylanaraps/fff.vim')
Plug('junegunn/fzf', { ['dir'] = '~/.fzf', ['do'] = './install --all' })

vim.call('plug#end')

require("nnn").setup()
--local function copy_to_clipboard(lines)
--	local joined_lines = table.concat(lines, "\n")
--	vim.fn.setreg("+", joined_lines)
--end
--
--require("nnn").setup({
--	command = "nnn -o -C",
--	set_default_mappings = 0,
--	replace_netrw = 1,
--	action = {
--		["<c-t>"] = "tab split",
--		["<c-s>"] = "split",
--		["<c-v>"] = "vsplit",
--		["<c-o>"] = copy_to_clipboard,
--	},
--})

