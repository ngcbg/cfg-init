---@diagnostic disable: undefined-doc-name
---@type ChadrcConfig
local M = {}

--M.ui = { theme = 'nord' }
M.mappings = require "custom.mappings"
M.plugins = "custom.plugins"
M.options = require "custom.options"
M.ui = {
    telescope = { style = "bordered" },
    changed_themes = {
      nord = {
        base_16 = {
          base03 = "#9ABB2B",
          base08 = "#88C0C9",
          base09 = "#EBCB8B",
          base0D = "#D6E8F4",
          base0E = "#81A1C1",
      },
        base_30 = {
           grey_fg = "#757B82",
--         grey_fg = "#788491",
      },
      polish_hl = {
        ["@punctuation.bracket"] = { fg = "#ABB2BF" },
        ["@punctuation.delimiter"] = { fg = "#6484A4" },
        ["@comment"] = { fg = "#757b82",italic = true },
      }
    }
  },
    theme = 'nord',
    statusline = { theme = "vscode_colored", separator_style = "round" },
    nvdash = { header = {
	[[                                   ]],
	[[  ▐ ▄ ▄▄▄ .       ▌ ▐·▪  • ▌ ▄ ·.  ]],
	[[ •█▌▐█▀▄.▀·▪     ▪█·█▌██ ·██ ▐███▪ ]],
	[[ ▐█▐▐▌▐▀▀▪▄ ▄█▀▄ ▐█▐█•▐█·▐█ ▌▐▌▐█· ]],
	[[ ██▐█▌▐█▄▄▌▐█▌.▐▌ ███ ▐█▌██ ██▌▐█▌ ]],
	[[ ▀▀ █▪ ▀▀▀  ▀█▄▀▪. ▀  ▀▀▀▀▀  █▪▀▀▀ ]],
	[[                                   ]],
  }
 }
}

--M.base46 = {
--}

return M
