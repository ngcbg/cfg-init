local M = {}
vim.g.mapleader = " "
M.abc = {

  n = {
     ["<C-s>"] = {"<cmd> Telescope <CR>", "Telescope"},
  },

    vim.keymap.set("n", "J", "mzJ`z"),
    vim.keymap.set("n", "{", "{zz"),
    vim.keymap.set("n", "}", "}zz"),
    vim.keymap.set("n", "<C-u>", "<C-u>zz"),
    vim.keymap.set("n", "<C-d>", "<C-d>zz"),
    vim.keymap.set("n", "n", "nzzzv"),
    vim.keymap.set("n", "N", "Nzzzv"),
    vim.keymap.set("x", "<C-p>", [["_dP]]),
    vim.keymap.set("n", "<C-q>", "<cmd>bdelete<CR>"),
    vim.keymap.set("n", "<C-b>", "<cmd>Telescope buffers<CR>", { noremap = true, silent = true }),
    vim.keymap.set("n", "<C-x>", "<cmd>Telescope dir live_grep<CR>", { noremap = true, silent = true }),
    vim.keymap.set("n", "<C-f>", "<cmd>Telescope dir find_files<CR>", { noremap = true, silent = true }),
    vim.keymap.set("n", "<C-A-h>", "<cmd>Telescope oldfiles<CR>", { noremap = true, silent = true }),

    vim.keymap.set("i", "<A-j>", "<C-o>j", { desc = "use Alt key + j in insert mode to retain vim-motion"}),
    vim.keymap.set("i", "<A-h>", "<C-o>h", { desc = "use Alt key + h in insert mode to retain vim-motion"}),
    vim.keymap.set("i", "<A-k>", "<C-o>k", { desc = "use Alt key + k in insert mode to retain vim-motion"}),
    vim.keymap.set("i", "<A-l>", "<C-o>l", { desc = "use Alt key + l in insert mode to retain vim-motion"}),

  --Incremet numbers
    vim.keymap.set("n", "<leader>+", "<C-a>", { desc = "Incremet number" }),
    vim.keymap.set("n", "<leader>-", "<C-x>", { desc = "Decremet number" }),

-- Windows managements
    vim.keymap.set("n", "<leader>sv", "<C-w>v", { desc = "Split window vertically" }),
    vim.keymap.set("n", "<leader>sh", "<C-w>s", { desc = "Split window horizontaly" }),
    vim.keymap.set("n", "<leader>se", "<C-w>=", { desc = "Make splits equal size" }),
    vim.keymap.set("n", "<leader>sx", "<cmd>close<CR>", { desc = "Close current split" }),

    -- vim.keymap.set("n", "<A-j>", "<C-W>j", { desc = "use Alt key + j in normal mode to switch between splits"}),
    -- vim.keymap.set("n", "<A-h>", "<C-W>h", { desc = "use Alt key + h in normal mode to switch between splits"}),
    -- vim.keymap.set("n", "<A-k>", "<C-W>k", { desc = "use Alt key + k in normal mode to switch between splits"}),
    -- vim.keymap.set("n", "<A-l>", "<C-W>l", { desc = "use Alt key + l in normal mode to switch between splits"}),

  --nvim-tree & nnn
    vim.keymap.set("n", "<leader>ee", "<cmd>NvimTreeToggle<CR>", { desc = "Toggle file explorer" }), -- toggle file explorer
    vim.keymap.set("n", "<leader>ef", "<cmd>NvimTreeFindFileToggle<CR>", { desc = "Toggle file explorer on current file" }), -- toggle file explorer on current file
    vim.keymap.set("n", "<leader>ec", "<cmd>NvimTreeCollapse<CR>", { desc = "Collapse file explorer" }), -- collapse file explorer
    vim.keymap.set("n", "<leader>er", "<cmd>NvimTreeRefresh<CR>", { desc = "Refresh file explorer" }), -- refresh file explorervim.keymap.set("n", "<leader>ec", "<cmd>NvimTreeCollapse<CR>", { desc = "Collapse file explorer" }),
    vim.keymap.set("n", "<leader>en", "<cmd>NnnPicker<CR>", { desc = "Show NnnPicker" }),
    vim.keymap.set("n", "<leader>em", "<cmd>NnnExplorer<CR>", { desc = "Show NnnExplorer" }),

  -- Telescope grep string   
    vim.keymap.set("n", "<leader>fc", "<cmd>Telescope grep_string<CR>", { desc = "grep string" }),
    -- vim.keymap.set("n", "<leader>", ":nohl <CR>");

  vim.api.nvim_set_keymap("n", "<C-a>", "", {
    desc = "Toggle Ranger FM",
    noremap = true,
    callback = function()
      require("ranger-nvim").open(true)
    end}),

  i = {
     ["jj"] = { "<ESC>", "escape insert mode" , opts = { nowait = true }},
  }
}

return M
