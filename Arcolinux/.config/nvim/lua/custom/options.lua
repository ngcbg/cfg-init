vim.cmd("let g:netrw_liststyle = 3")

local opt = vim.opt

opt.guicursor = ""
--vim.opt.nu = true
opt.relativenumber = true
opt.number = true
opt.autochdir = true

-- tabs & indent
opt.tabstop = 4 -- 4 spaces for tabs
opt.softtabstop = 4
opt.shiftwidth = 4 -- 4 spaces for indent width
opt.expandtab = true -- expand tab to spaces
opt.autoindent = true -- copy indent form current line when starting a new one
opt.cursorline = true
opt.wrap = false

-- turn on termguicolors
opt.termguicolors = true
opt.background = "dark" -- colorschemes that could be light or dark wiil be made dark
opt.signcolumn = "yes" -- show sign column so that text doesn't shift

-- Serach settings
opt.ignorecase = true -- ignore case when search
opt.smartcase = true -- When mixed case in search, assumes you want case-sensitive
opt.hlsearch = true
opt.incsearch = true

-- Clipboard
opt.clipboard:append("unnamedplus") -- use system clipboard as default register

-- Backspace
opt.backspace = "indent,eol,start" -- allow backspace on indent, EOL or insert mode start possition

-- Split windows
opt.splitright = true -- split vertical window to the right
opt.splitbelow = true -- split horizontal window to the bottom

opt.swapfile = false
opt.backupdir = os.getenv("HOME") .. "/.vimbackup/nvbackup"
opt.backup = true
opt.undofile = true
opt.undodir = os.getenv("HOME") .. "/.vimbackup/nvbackup"
opt.scrolloff = 8
opt.isfname:append("@-@")
--vim.opt.updatetime = 50
--vim.opt.colorcolumn = "80"
