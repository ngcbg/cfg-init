local plugins = {

  {
    "neovim/nvim-lspconfig",
    config = function()
      require "plugins.configs.lspconfig"
      require "custom.configs.lspconfig"
    end,
  },

  {
  "kelly-lin/ranger.nvim", lazy = false,
    config = function()
      require("ranger-nvim").setup({ replace_netrw = true,
        ui = {
          border = "none",
          height = 1,
          width = 1,
          x = 0.5,
          y = 0.5,
         }
    })
    end,
  },

  {
  "princejoogie/dir-telescope.nvim",
  -- telescope.nvim is a required dependency
  requires = {"nvim-telescope/telescope.nvim"},
  config = function()
    require("dir-telescope").setup({
      -- these are the default options set
      hidden = true,
      no_ignore = false,
      show_preview = true,
    })
    end,
  },
--  {
--  "jackMort/ChatGPT.nvim",
--    event = "VeryLazy",
--    config = function()
--      require("chatgpt").setup()
--    end,
--    dependencies = {
--      "MunifTanjim/nui.nvim",
--      "nvim-lua/plenary.nvim",
--      "nvim-telescope/telescope.nvim"
--    }
--  },
  {
    "RRethy/vim-illuminate",
    event = { "CursorHold", "CursorHoldI" },
    dependencies = "nvim-treesitter",
    config = function()
      require("illuminate").configure {
        under_cursor = true,
        max_file_lines = nil,
        delay = 100,
        providers = {
          "lsp",
          "treesitter",
          "regex",
        },
        filetypes_denylist = {
          "NvimTree",
          "Trouble",
          "Outline",
          "TelescopePrompt",
          "Empty",
          "dirvish",
          "fugitive",
          "alpha",
          "packer",
          "neogitstatus",
          "spectre_panel",
          "toggleterm",
          "DressingSelect",
          "aerial",
        },
      }
    end,
  },

  {
  "vimwiki/vimwiki",
      event = "BufEnter *.wiki",
      keys = { "<leader>ww", "<leader>wt" },
      init = function()
        vim.g.vimwiki_folding = ""
        vim.g.vimwiki_list = {
          {
              path = '~/.vimwiki',
              syntax = 'default',
              ext = '.wiki',
           },
           {
              path = '~/Documents/wiki',
              syntax = 'markdown',
              ext = '.md',
           },
       }
   end,
  },

  {
  "stevearc/dressing.nvim",
      event = "VeryLazy",
  },

  {
    "machakann/vim-highlightedyank", lazy = false
  },
  {
    "m-demare/hlargs.nvim",
    event = "BufWinEnter",
    config = function()
      require("hlargs").setup {
        hl_priority = 200,
      }
    end,
  },

  {
  "nvim-telescope/telescope.nvim",
  dependencies = {
    {
      "debugloop/telescope-undo.nvim",
      config = function()
        local map = vim.keymap.set
        map("n", "<leader>fu", "<CMD>Telescope undo<CR>", { desc = "Find undo" })

        require("telescope").load_extension "undo"
      end,
      },
    },
  }
}

return plugins

