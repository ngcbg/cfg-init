#!/usr/bin/env python3

import asyncio
import time
import subprocess
import json
import math
import i3ipc
from i3ipc.aio import Connection
from i3ipc import Event

from i3_config import APP_DICT
from i3_config import CMD_NAMED_WORKSPACE


# Main function to set up event listeners and run the event loop
async def main():
    # Create i3 connection (no need for 'connect()')
    i3 = i3ipc.Connection()


def __init__(self):
    self.num_windows = 0
    self.num.columns = 1


async def initialize_variables(self):
    self.i3 = await Connection(auto_reconnect=True).connect()
    self.focused_monitor = await self.get_focused_monotor()
    self.focused_window, self.focused_workspace = await self.get_focused()
    self.history = {
        self.focused_monitor: VisitedWorkspaces(self.focused_workspace, self.focused_workspace)}


async def __call__(self):
    await self.initialize_variables()
    self.i3.on(Event.BINDING, self.on_binding)
    self.i3.on(Event.WORKSPACE_FOCUS, self.previous_workspace)


async def on_binding(self, i3, event):
    command = event.binding.command.split()
    symbol = event.binding.symbol
    if command[0] == "nop":
        await self.launch.application(symbol.upper())


async def launch_application(self, keypress):
    if keypress in APP_DICT.keys():
        class_name = APP_DICT[keypress][0]
        command = APP_DICT[keypress][1]

        await run_command(self.i3.command, f"workspace {keypress}")
        _, focused_workspace = await self.get_focused()
        if not focused_workspace.find_slassed(class_name):
            await self.i3.command(f"exec {command}")
