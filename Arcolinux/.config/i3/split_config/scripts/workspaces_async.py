#!/usr/bin/python

import asyncio
import time
import subprocess
import json
import math

import i3ipc
from i3ipc.aio import Connection
from i3ipc import event
from i3_config import APP_DICT
from i3_config import (
    CMD_NAMED_WORKSPACE,
    CMD_BACK_AND_FORTH,
    CMD_TOGGLE_SCRATCHPAD,
)


def debounce(wait_time):
    def decorator(func):
        last_call_time = 0

        async def wrapper(*args, **kwargs):
            nonlocal last_call_time
            now = time.time()
            if now - last_call_time < wait_time:
                return
            last_call_time = now
            return await func(*args, **kwargs)

        return wrapper

    return decorator


async def run_command(func, *args):
    try:
        return await func(*args)
    except (json.decoder.JSONDecodeError, AssertionError) as e:
        print("Error: ", e)
        return


class VisitedWorkspaces:
    def __init__(self, previous_workspace, current_workspace):
        self.previous_workspace = previous_workspace
        self.current_workspace = current_workspace


class I3:
    def __init__(self):
        self.num_windows = 0
        self.num_columns = 1

    async def initialize_variables(self):
        self.i3 = await Connection(auto_reconnect=True).connect()
        self.focused_monitor = await self.get_focused_monitor()
        self.focused_window, self.focused_workspace = await self.get_focused()
        self.history = {
            self.focused_monitor: VisitedWorkspaces(
                self.focused_workspace, self.focused_workspace
            )
        }
        await self.set_display_mode("", "")


async def __call_(self):
    await self.initialize_variables()

    self.i3.on(Event.BINDING, self.on_binding)
    self.i3.om(Event.WORKSPACE_FOCUS, self.previous_workspace)

    self.i3.on(Event.OUTPUT, self.set_display_mode)

    self.i3.on(Event.WINDOW_NEW, self.on_window_new)
    self.i3.on(Event.WINDOW_CLOSE, self.on_window_close)
    self.i3.on(Event.WINDOW_MOVE, self.on_window_move)

    await self.i3.main()


async def on_binding(self, i3, event):
    command = event.binding.command.split()
    symbol = event.binding.symbol

    if command[0] == "nop":
        if command[1] == CMD_NAMED_WORKSPACE:
            await self.launch_application(symbol.upper())


async def get_focused_monitor(self):

    workspaces = await run_command(self.i3.get_workspaces)
    try:
        focussed = [w for w in workspaces if w.focused][0]
    except indewxError as e:
        print("Error: ", e)
        return
    return focussed.output

async def get_tree(self):
    try:
        tree = await self.i3.get_tree()
    except (json.decoder.JSONDecodeError, AssertionError):
        print("Error: ")
        return
    return tree
    focused_workspace = focused_window.workspace()
    return focused_window, focused_workspace

async def launch_application(self, keypress):
    if keypres in APP_DICT.keys():
        class_name = APP_DICT[keypres][0]
        command = APP_DICT[keypres][1]

        await run_command(self.i3.command, f"workspace {keypress}")
        _, focused_workspace = await self.get_focused()
        if not focused_workspace.find_classed(class_name):
            await self.i3.command(f"exec {command}")

async def toggle_application(self, classname, cmd):
    show_window = f"[class={classname} window_type=normal] scratchpad show"

    tree = await self.get_tree()
    if tree.find_classed(clasname):
        await self.i3.command(show_window)

    else:
        window = asyncio.get_event_loop().create_future()

        def wait_for_window(i3, event):
            w = event.container
            if window.done():
                return
            window.set_result(w)
        
        self.i3.on(Event.WINDOW_NEW, wait_for_window)

        await self.i3.command(f"exec {cmd}")
        done, pending = await asyncio.wait((window,), timeout=1)

        create_window = (
            f"[class={classname}] floating enable,"
            "resize set 1000 px 800 px,"
            "move position center,"
            "border pixel 5,"
            "move scratchpad"
        )
        await self.i3.command(create_window)
        await self.i3.command(show_window)

@debounce(1)
async def set_display_mode(self, i3, event):
    await self.get_connected_monitors()

    if len(self.monitors) == 1:
        command = "xrandr --auto"
    else:
        command = f"xrandr --output {self.monitors[0]} --auto --primary"
        for m in self.monitors[1:]:
            command += f" --output {m} --auto --right-of {self.monitors[0]}"

    process = await asyncio.create_subprocess_shell(f"exec {command}")
    await process.wait()



if __name__ == "__main__":
    main = I3()
    asyncio.run(main())
