############################
# I3 autostart script   #
############################

#!/bin/bash

function run {
 if ! pgrep $1 ;
  then
    $@&
 fi
}

xset b off
xrdb -merge ~/.Xresources &
. ~/.bashrc
export DE=xfce
export XDG_CURRENT_DESKTOP=xfce
export WM=I3
export COLORTERM=truecolor
setxkbmap -layout us,bg -variant ,phonetic -option grp:alt_shift_toggle

# Set personalized GTK elemnts per WM
sed -i "s/.*gtk-theme-name=.*/gtk-theme-name=Fleon/g" ~/.config/gtk-3.0/settings.ini
sed -i "s/.*gtk-icon-theme-name=.*/gtk-icon-theme-name=Bluecurve8/g" ~/.config/gtk-3.0/settings.ini
sed -i "s/.*gtk-theme-name=.*/gtk-theme-name=\"Fleon\"/g" ~/.gtkrc-2.0
sed -i "s/.*gtk-icon-theme-name=.*/gtk-icon-theme-name=\"Bluecurve8\"/g" ~/.gtkrc-2.0

if [ -d /usr/share/fonts ]; then
    for i in /usr/share/fonts/*; do
        xset fp+ $i
    done
    xset fp rehash
fi

if [ -d /home/ngc/.fonts ]; then
    for i in /home/ngc/.fonts/*; do
        xset fp+ $i
    done
    xset fp rehash
fi

run xrandr --output DP-2 --primary --mode 2560x1440 --pos 0x0 --rotate normal --output DP-0 --mode 2560x1440 --pos 2560x0 --scale 0.9x0.9 --rotate right
run picom -b --vsync-use-glfinish --experimental-backends --config ~/picom.NGC &

run /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &
#run /usr/bin/sxhkd -c ~/.config/cwm/sxhkd/sxhkdrc &
run /usr/bin/dunst &
run /usr/bin/xfce4-power-manager --daemon &
run /usr/bin/nm-applet &
run /usr/bin/volumeicon &
run /usr/bin/numlockx on &
run /usr/bin/autocutsel &
pgrep urxvtd 2> /dev/null || urxvtd -q -o -f &
run /usr/bin/xsct 6000 1>/dev/null &
/home/ngc/.bin/nitro-i3.sh

