# vim:nospell:ft=sh

set -g -x PATH /usr/bin $PATH
set HISTCONTROL ignoreboth:erasedups

#set VIRTUAL_ENV_DISABLE_PROMPT "1"
if not status --is-interactive
  exit
end

# reload fish config
function reload
    exec fish
    set -l config (status -f)
    echo "reloading: $config"
end

# User paths
set -e fish_user_paths
set -U fish_user_paths $HOME/.bin $HOME/.local/bin $HOME/Applications $fish_user_paths

# sets tools
set -x EDITOR nvim
set -x VISUAL nvim
set -g TERMINAL kitty
set WM i3
set DE xfce
set COLORTERM truecolor
# Sets the terminal type for proper colors
#set TERM "xterm-256color"
#set -x TERM kitty
set -x PATH /home/ngc/perl5/bin $PATH 2>/dev/null;
set -q PERL5LIB; and set -x PERL5LIB /home/ngc/perl5/lib/perl5:$PERL5LIB;
set -q PERL5LIB; or set -x PERL5LIB /home/ngc/perl5/lib/perl5;
set -q PERL_LOCAL_LIB_ROOT; and set -x PERL_LOCAL_LIB_ROOT /home/ngc/perl5:$PERL_LOCAL_LIB_ROOT;
set -q PERL_LOCAL_LIB_ROOT; or set -x PERL_LOCAL_LIB_ROOT /home/ngc/perl5;
set -x PERL_MB_OPT --install_base\ \"/home/ngc/perl5\";
set -x PERL_MM_OPT INSTALL_BASE=/home/ngc/perl5;
# Prevent directories names from being shortened
set fish_prompt_pwd_dir_length 0

fzf --fish | source
set -x FZF_DEFAULT_OPTS " \
  --color=16,header:13,info:5,pointer:3,marker:9,spinner:1,prompt:5,fg:7,hl:14,fg+:3,hl+:9 \
  --inline-info --tiebreak=end,length --bind=shift-tab:toggle-down,tab:toggle-up"

if type rg &> /dev/null
    set -g FZF_DEFAULT_COMMAND rg --files --hidden
    set -g FZF_DEFAULT_OPTS -m
end

#set -x MANPAGER "sh -c 'col -bx | bat -l man -p'"
#set -x MANPAGER less
#set -x MANPAGER most
#set -x MANPAGER "nvim --clean +Man!"
set -x MANPAGER "nvim +Man!"
set -g theme_nerd_fonts yes

if status --is-login
    set -gx PATH $PATH ~/.bin
end

if status --is-login
    set -gx PATH $PATH ~/.local/bin
end

if type -q bat
    alias cat="bat --paging=never"
end
set -x BAT_THEME OneHalfDark

if command -sq fzf && type -q fzf_configure_bindings
  fzf_configure_bindings --directory=\ct
end

if not set -q -g fish_user_abbreviations
  set -gx fish_user_abbreviations
end

if which tree >/dev/null
    function l1;  tree -C --dirsfirst -ChFL 1 $argv; end
    function l2;  tree -C --dirsfirst -ChFL 2 $argv; end
    function l3;  tree -C --dirsfirst -ChFL 3 $argv; end
    function ll1; tree -C --dirsfirst -ChFupDaL 1 $argv; end
    function ll2; tree -C --dirsfirst -ChFupDaL 2 $argv; end
    function ll3; tree -C --dirsfirst -ChFupDaL 3 $argv; end
end

function le; pygmentize -P style=nord $argv | /usr/bin/less -iMFNceqwRSX -x2 --use-color; end

if type -q direnv
    eval (direnv hook fish)
end

#Aliases
#fix obvious typo's
alias cd..='cd ..'
alias pdw="pwd"
#alias udpate='sudo pacman -Syyu'
alias upate='sudo pacman -Syyu'
#alias ll="ls -alh"
alias ll="eza -lah -s name --group-directories-first --octal-permissions --icons --git"
alias lll="exa -lah -s name --group-directories-first --octal-permissions --icons --git"
alias less="less -iMFNceqwRSX -x2 --use-color"
alias pygmentize="pygmentize -P style=nord"
#alias m="mcedit"
#alias v="vim"
#alias n="nano"
#alias les='less.sh'
#alias mc='bass source ~/.bin/mc-wrapper.sh'
#alias less='/home/ngc/.bin/less.sh'
set PAGER less

## Colorize the grep command output for ease of use (good for log files)##
alias grep='rg'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

# Tools
alias news="newsboat"
alias todo="remind -n ~/Documents/reminders.txt"
#readable output
#alias df='df -h'

#pacman unlock
alias unlock="sudo rm /var/lib/pacman/db.lck"
alias rmpacmanlock="sudo rm /var/lib/pacman/db.lck"

#arcolinux logout unlock
alias rmlogoutlock="sudo rm /tmp/arcologout.lock"

#continue download
alias wget="wget -c"

#userlist
#alias userlist="cut -d: -f1 /etc/passwd"

#merge new settings
alias merge="xrdb -merge ~/.Xresources"

# Aliases for software managment
# pacman or pm
alias pacman='sudo pacman --color auto'
#alias update='sudo pacman -Syyu'

# yay as aur helper - updates everything
#alias pksyua="yay -Syu --noconfirm"
#alias upall="yay -Syu --noconfirm"

#ps
alias psa="ps auxf"
alias psgrep="ps aux | grep -v grep | grep -i -e VSZ -e"

#grub update
alias update-grub="sudo grub-mkconfig -o /boot/grub/grub.cfg"

#add new fonts
alias update-fc='sudo fc-cache -fv'

#copy/paste all content of /etc/skel over to home folder - backup of config created - beware
#alias skel='cp -Rf ~/.config ~/.config-backup-$(date +%Y.%m.%d-%H.%M.%S) && cp -rf /etc/skel/* ~'
#backup contents of /etc/skel to hidden backup folder in home/user
#alias bupskel='cp -Rf /etc/skel ~/.skel-backup-$(date +%Y.%m.%d-%H.%M.%S)'

#copy bashrc-latest over on bashrc - cb= copy bashrc
alias cb='sudo cp /etc/skel/.bashrc ~/.bashrc && source ~/.bashrc'
#copy /etc/skel/.zshrc over on ~/.zshrc - cb= copy zshrc
alias cz='sudo cp /etc/skel/.zshrc ~/.zshrc && source ~/.zshrc'

#switch between bash and zsh
alias tofish="sudo chsh $USER -s /usr/bin/fish && echo 'Please log out' && notify-send 'Please Log out'" 
alias tobash="sudo chsh $USER -s /bin/bash && echo 'Please log out' && notify-send 'Please Log out'"
alias tozsh="sudo chsh $USER -s /bin/zsh && echo 'Please log out' && notify-send 'Please Log out'"

#quickly kill conkies
alias kc='killall conky'

#hardware info --short
alias hw="hwinfo --short"

#skip integrity check
alias yayskip='yay -S --mflags --skipinteg'
alias trizenskip='trizen -S --skipinteg'

#check vulnerabilities microcode
alias microcode='grep . /sys/devices/system/cpu/vulnerabilities/*'

#get fastest mirrors in your neighborhood
alias mirror="sudo reflector -f 30 -l 30 --number 10 --verbose --save /etc/pacman.d/mirrorlist"
alias mirrord="sudo reflector --latest 50 --number 20 --sort delay --save /etc/pacman.d/mirrorlist"
alias mirrors="sudo reflector --latest 50 --number 20 --sort score --save /etc/pacman.d/mirrorlist"
alias mirrora="sudo reflector --latest 50 --number 20 --sort age --save /etc/pacman.d/mirrorlist"

#mounting the folder Public for exchange between host and guest on virtualbox
alias vbm="sudo /usr/local/bin/arcolinux-vbox-share"

#shopt
#shopt -s autocd # change to named directory
#shopt -s cdspell # autocorrects cd misspellings
#shopt -s cmdhist # save multi-line commands in history as single line
#shopt -s dotglob
#shopt -s histappend # do not overwrite history
#shopt -s expand_aliases # expand aliases

#youtube-dl
alias yta-aac="youtube-dl --extract-audio --audio-format aac "
alias yta-best="youtube-dl --extract-audio --audio-format best "
alias yta-flac="youtube-dl --extract-audio --audio-format flac "
alias yta-m4a="youtube-dl --extract-audio --audio-format m4a "
alias yta-mp3="youtube-dl --extract-audio --audio-format mp3 "
alias yta-opus="youtube-dl --extract-audio --audio-format opus "
alias yta-vorbis="youtube-dl --extract-audio --audio-format vorbis "
alias yta-wav="youtube-dl --extract-audio --audio-format wav "

alias ytv-best="youtube-dl -f bestvideo+bestaudio "

#Recent Installed Packages
alias rip="expac --timefmt='%Y-%m-%d %T' '%l\t%n %v' | sort | tail -200 | nl"
alias riplong="expac --timefmt='%Y-%m-%d %T' '%l\t%n %v' | sort | tail -3000 | nl"

#iso and version used to install ArcoLinux
alias iso="cat /etc/dev-rel | awk -F '=' '/ISO/ {print $2}'"

#Cleanup orphaned packages
alias cleanup='sudo pacman -Rns (pacman -Qtdq)'

#clear
alias clean="clear; seq 1 (tput cols) | sort -R | sparklines | lolcat"

#get the error messages from journalctl
alias jctl="journalctl -p 3 -xb"

#nano for important configuration files
#know what you do in these files
alias nlightdm="sudo nano /etc/lightdm/lightdm.conf"
alias npacman="sudo nano /etc/pacman.conf"
alias ngrub="sudo nano /etc/default/grub"
alias nmkinitcpio="sudo nano /etc/mkinitcpio.conf"
alias nslim="sudo nano /etc/slim.conf"
alias noblogout="sudo nano /etc/oblogout.conf"
alias nmirrorlist="sudo nano /etc/pacman.d/mirrorlist"
alias nconfgrub="sudo nano /boot/grub/grub.cfg"
alias bls="betterlockscreen -u /usr/share/backgrounds/arcolinux/"

#reading logs with bat
alias lcalamares="bat /var/log/Calamares.log"
alias lpacman="bat /var/log/pacman.log"
alias lxorg="bat /var/log/Xorg.0.log"
alias lxorgo="bat /var/log/Xorg.0.log.old"

#gpg
#verify signature for isos
alias gpg-check="gpg2 --keyserver-options auto-key-retrieve --verify"
alias fix-gpg-check="gpg2 --keyserver-options auto-key-retrieve --verify"
#receive the key of a developer
alias gpg-retrieve="gpg2 --keyserver-options auto-key-retrieve --receive-keys"
alias fix-gpg-retrieve="gpg2 --keyserver-options auto-key-retrieve --receive-keys"
alias fix-keyserver="[ -d ~/.gnupg ] || mkdir ~/.gnupg ; cp /etc/pacman.d/gnupg/gpg.conf ~/.gnupg/ ; echo 'done'"

#fixes
alias fix-permissions="sudo chown -R $USER:$USER ~/.config ~/.local"
alias keyfix="/usr/local/bin/arcolinux-fix-pacman-databases-and-keys"
alias key-fix="/usr/local/bin/arcolinux-fix-pacman-databases-and-keys"
alias keys-fix="/usr/local/bin/arcolinux-fix-pacman-databases-and-keys"
alias fixkey="/usr/local/bin/arcolinux-fix-pacman-databases-and-keys"
alias fixkeys="/usr/local/bin/arcolinux-fix-pacman-databases-and-keys"
alias fix-key="/usr/local/bin/arcolinux-fix-pacman-databases-and-keys"
alias fix-keys="/usr/local/bin/arcolinux-fix-pacman-databases-and-keys"
alias fix-sddm-config="/usr/local/bin/arcolinux-fix-sddm-config"
alias fix-pacman-conf="/usr/local/bin/arcolinux-fix-pacman-conf"
alias fix-pacman-keyserver="/usr/local/bin/arcolinux-fix-pacman-gpg-conf"

#shutdown or reboot
alias ssn="sudo shutdown now"
#alias sr="sudo reboot"

export GTK_OVERLAY_SCROLLING=0
#export QT_AUTO_SCREEN_SCALE_FACTOR=1
#export QT_SCALE_FACTOR=1

# POwerline
set fish_function_path $fish_function_path "/usr/share/powerline/bindings/fish"
#source /usr/share/powerline/bindings/fish/powerline-setup.fish
#powerline-setup

# Enable command history search via fzf.                                    
function reverse_history_search                                    
  history | fzf --no-sort | read -l command                                    
  if test $command                                    
    commandline -rb $command                                    
  end                                    
end                                    
                                    
function fish_user_key_bindings                                    
  bind -M default / reverse_history_search                                    
end                               

# Emulates vim's cursor shape behavior
# Set the normal and visual mode cursors to a block
set fish_cursor_default block
# Set the insert mode cursor to a line
set fish_cursor_insert block
# Set the replace mode cursor to an underscore
set fish_cursor_replace_one underscore
# The following variable can be used to configure cursor shape in
# visual mode, but due to fish_cursor_default, is redundant here
set fish_cursor_visual line

if test "$TERM" = "xterm-kitty"
    fastfetch --logo-width 32 --logo-height 14 
else
    neofetch
end
#set -g fish_greeting "Howdy NGC!"
#echo $fish_greeting

abbr -a -- grmc 'git rm --cached' # imported from a universal variable, see `help abbr`
abbr -a -- grm 'git rm' # imported from a universal variable, see `help abbr`
abbr -a -- gcfx 'git commit --fixup' # imported from a universal variable, see `help abbr`
abbr -a -- grhpa 'git reset --patch' # imported from a universal variable, see `help abbr`
abbr -a -- grhh 'git reset --hard' # imported from a universal variable, see `help abbr`
abbr -a -- grbmi 'git rebase (__git.default_branch) --interactive' # imported from a universal variable, see `help abbr`
abbr -a -- grbdi 'git rebase develop --interactive' # imported from a universal variable, see `help abbr`
abbr -a -- gra 'git remote add' # imported from a universal variable, see `help abbr`
abbr -a -- gpo! 'git push --force-with-lease origin' # imported from a universal variable, see `help abbr`
abbr -a -- grbmia 'git rebase (__git.default_branch) --interactive --autosquash' # imported from a universal variable, see `help abbr`
abbr -a -- gp! 'git push --force-with-lease' # imported from a universal variable, see `help abbr`
abbr -a -- gfhs 'git flow hotfix start' # imported from a universal variable, see `help abbr`
abbr -a -- grh 'git reset' # imported from a universal variable, see `help abbr`
abbr -a -- gcm 'git commit -m' # imported from a universal variable, see `help abbr`
abbr -a -- gm 'git merge' # imported from a universal variable, see `help abbr`
abbr -a -- glr 'git pull --rebase' # imported from a universal variable, see `help abbr`
abbr -a -- gmt 'git mergetool --no-prompt' # imported from a universal variable, see `help abbr`
abbr -a -- gl 'git pull' # imported from a universal variable, see `help abbr`
abbr -a -- gloo git\ log\ --graph --all --pretty=format:\'\%C\(yellow\)\%h\ \%Cred\%ad\ \%Cblue\%an\%Cgreen\%d\ \%Creset\%s\'\ --date=short # imported from a universal variable, see `help abbr`
abbr -a -- gau 'git add --update' # imported from a universal variable, see `help abbr`
abbr -a -- gunignore 'git update-index --no-assume-unchanged' # imported from a universal variable, see `help abbr`
abbr -a -- grbm 'git rebase (__git.default_branch)' # imported from a universal variable, see `help abbr`
abbr -a -- gloga 'git log --oneline --decorate --color --graph --all' # imported from a universal variable, see `help abbr`
abbr -a -- glog 'git log --oneline --decorate --color --graph' # imported from a universal variable, see `help abbr`
abbr -a -- glod 'git log --oneline --decorate --color develop..' # imported from a universal variable, see `help abbr`
abbr -a -- glo 'git log --oneline --decorate --color' # imported from a universal variable, see `help abbr`
abbr -a -- glom 'git log --oneline --decorate --color (__git.default_branch)..' # imported from a universal variable, see `help abbr`
abbr -a -- gcav! 'git commit -a -v --no-verify --amend' # imported from a universal variable, see `help abbr`
abbr -a -- grba 'git rebase --abort' # imported from a universal variable, see `help abbr`
abbr -a -- gfst 'git flow support track' # imported from a universal variable, see `help abbr`
abbr -a -- gfbt 'git flow bugfix track' # imported from a universal variable, see `help abbr`
abbr -a -- grmv 'git remote rename' # imported from a universal variable, see `help abbr`
abbr -a -- grs 'git restore' # imported from a universal variable, see `help abbr`
abbr -a -- gpo 'git push origin' # imported from a universal variable, see `help abbr`
abbr -a -- gfo 'git fetch origin' # imported from a universal variable, see `help abbr`
abbr -a -- gwch 'git whatchanged -p --abbrev-commit --pretty=medium' # imported from a universal variable, see `help abbr`
abbr -a -- grrm 'git remote remove' # imported from a universal variable, see `help abbr`
abbr -a -- gfht 'git flow hotfix track' # imported from a universal variable, see `help abbr`
abbr -a -- glgg 'git log --graph' # imported from a universal variable, see `help abbr`
abbr -a -- gfft 'git flow feature track' # imported from a universal variable, see `help abbr`
abbr -a -- gffs 'git flow feature start' # imported from a universal variable, see `help abbr`
abbr -a -- gts 'git tag -s' # imported from a universal variable, see `help abbr`
abbr -a -- gss 'git status -s' # imported from a universal variable, see `help abbr`
abbr -a -- gfb 'git flow bugfix' # imported from a universal variable, see `help abbr`
abbr -a -- gfa 'git fetch --all --prune' # imported from a universal variable, see `help abbr`
abbr -a -- gca! 'git commit -v -a --amend' # imported from a universal variable, see `help abbr`
abbr -a -- gdwc 'git diff --word-diff --cached' # imported from a universal variable, see `help abbr`
abbr -a -- gpv 'git push --no-verify' # imported from a universal variable, see `help abbr`
abbr -a -- grbi 'git rebase --interactive' # imported from a universal variable, see `help abbr`
abbr -a -- gdw 'git diff --word-diff' # imported from a universal variable, see `help abbr`
abbr -a -- gbss 'git bisect start' # imported from a universal variable, see `help abbr`
abbr -a -- gcn! 'git commit -v --no-edit --amend' # imported from a universal variable, see `help abbr`
abbr -a -- gwt 'git worktree' # imported from a universal variable, see `help abbr`
abbr -a -- ga 'git add' # imported from a universal variable, see `help abbr`
abbr -a -- gbl 'git blame -b -w' # imported from a universal variable, see `help abbr`
abbr -a -- grbc 'git rebase --continue' # imported from a universal variable, see `help abbr`
abbr -a -- gfrs 'git flow release start' # imported from a universal variable, see `help abbr`
abbr -a -- gfr 'git flow release' # imported from a universal variable, see `help abbr`
abbr -a -- gsts 'git stash show --text' # imported from a universal variable, see `help abbr`
abbr -a -- gaa 'git add --all' # imported from a universal variable, see `help abbr`
abbr -a -- gstl 'git stash list' # imported from a universal variable, see `help abbr`
abbr -a -- gfs 'git flow support' # imported from a universal variable, see `help abbr`
abbr -a -- gpv! 'git push --no-verify --force-with-lease' # imported from a universal variable, see `help abbr`
abbr -a -- gbd 'git branch -d' # imported from a universal variable, see `help abbr`
abbr -a -- ggp! 'ggp --force-with-lease' # imported from a universal variable, see `help abbr`
abbr -a -- gstp 'git stash pop' # imported from a universal variable, see `help abbr`
abbr -a -- gbD 'git branch -D' # imported from a universal variable, see `help abbr`
abbr -a -- gds 'git diff --stat' # imported from a universal variable, see `help abbr`
abbr -a -- gfh 'git flow hotfix' # imported from a universal variable, see `help abbr`
abbr -a -- gswc 'git switch --create' # imported from a universal variable, see `help abbr`
abbr -a -- gbsg 'git bisect good' # imported from a universal variable, see `help abbr`
abbr -a -- gf 'git fetch' # imported from a universal variable, see `help abbr`
abbr -a -- gbsb 'git bisect bad' # imported from a universal variable, see `help abbr`
abbr -a -- gcf 'git config --list' # imported from a universal variable, see `help abbr`
abbr -a -- gpu 'ggp --set-upstream' # imported from a universal variable, see `help abbr`
abbr -a -- gfp 'git flow publish' # imported from a universal variable, see `help abbr`
abbr -a -- gba 'git branch -a -v' # imported from a universal variable, see `help abbr`
abbr -a -- grpo 'git remote prune origin' # imported from a universal variable, see `help abbr`
abbr -a -- df 'df -h' # imported from a universal variable, see `help abbr`
abbr -a -- gfrt 'git flow release track' # imported from a universal variable, see `help abbr`
abbr -a -- free '/usr/bin/free -mwtlh' # imported from a universal variable, see `help abbr`
abbr -a -- gca 'git commit -v -a' # imported from a universal variable, see `help abbr`
abbr -a -- gclean! 'git clean -dfx' # imported from a universal variable, see `help abbr`
abbr -a -- gcam 'git commit -a -m' # imported from a universal variable, see `help abbr`
abbr -a -- gbs 'git bisect' # imported from a universal variable, see `help abbr`
abbr -a -- gcav 'git commit -a -v --no-verify' # imported from a universal variable, see `help abbr`
abbr -a -- gclean 'git clean -di' # imported from a universal variable, see `help abbr`
abbr -a -- gcb 'git checkout -b' # imported from a universal variable, see `help abbr`
abbr -a -- gfm 'git fetch origin (__git.default_branch) --prune; and git merge FETCH_HEAD' # imported from a universal variable, see `help abbr`
abbr -a -- less 'le' # imported from a universal variable, see `help abbr`
abbr -a -- gcl 'git clone' # imported from a universal variable, see `help abbr`
abbr -a -- gcv 'git commit -v --no-verify' # imported from a universal variable, see `help abbr`
abbr -a -- gcan! 'git commit -v -a --no-edit --amend' # imported from a universal variable, see `help abbr`
abbr -a -- gp 'git push' # imported from a universal variable, see `help abbr`
abbr -a -- gclean!! 'git reset --hard; and git clean -dfx' # imported from a universal variable, see `help abbr`
abbr -a -- gcpc 'git cherry-pick --continue' # imported from a universal variable, see `help abbr`
abbr -a -- gupv 'git pull --rebase -v' # imported from a universal variable, see `help abbr`
abbr -a -- grbd 'git rebase develop' # imported from a universal variable, see `help abbr`
abbr -a -- glg 'git log --stat' # imported from a universal variable, see `help abbr`
abbr -a -- gapa 'git add --patch' # imported from a universal variable, see `help abbr`
abbr -a -- g git # imported from a universal variable, see `help abbr`
abbr -a -- gr 'git remote -vv' # imported from a universal variable, see `help abbr`
abbr -a -- gcom 'git checkout (__git.default_branch)' # imported from a universal variable, see `help abbr`
abbr -a -- gcpa 'git cherry-pick --abort' # imported from a universal variable, see `help abbr`
abbr -a -- gcount 'git shortlog -sn' # imported from a universal variable, see `help abbr`
abbr -a -- grbdia 'git rebase develop --interactive --autosquash' # imported from a universal variable, see `help abbr`
abbr -a -- gc! 'git commit -v --amend' # imported from a universal variable, see `help abbr`
abbr -a -- gfss 'git flow support start' # imported from a universal variable, see `help abbr`
abbr -a -- gap 'git apply' # imported from a universal variable, see `help abbr`
abbr -a -- gcod 'git checkout develop' # imported from a universal variable, see `help abbr`
abbr -a -- gignore 'git update-index --assume-unchanged' # imported from a universal variable, see `help abbr`
abbr -a -- gdto 'git difftool' # imported from a universal variable, see `help abbr`
abbr -a -- grev 'git revert' # imported from a universal variable, see `help abbr`
abbr -a -- grset 'git remote set-url' # imported from a universal variable, see `help abbr`
abbr -a -- grss 'git restore --source' # imported from a universal variable, see `help abbr`
abbr -a -- grup 'git remote update' # imported from a universal variable, see `help abbr`
abbr -a -- m mcedit # imported from a universal variable, see `help abbr`
abbr -a -- gsd 'git svn dcommit' # imported from a universal variable, see `help abbr`
abbr -a -- gupa 'git pull --rebase --autostash' # imported from a universal variable, see `help abbr`
abbr -a -- n nano # imported from a universal variable, see `help abbr`
abbr -a -- gff 'git flow feature' # imported from a universal variable, see `help abbr`
abbr -a -- gsr 'git svn rebase' # imported from a universal variable, see `help abbr`
abbr -a -- gst 'git status' # imported from a universal variable, see `help abbr`
abbr -a -- gwtlo 'git worktree lock' # imported from a universal variable, see `help abbr`
abbr -a -- gsu 'git submodule update' # imported from a universal variable, see `help abbr`
abbr -a -- gsuri 'git submodule update --recursive --init' # imported from a universal variable, see `help abbr`
abbr -a -- gtv 'git tag' # imported from a universal variable, see `help abbr`
abbr -a -- gup 'git pull --rebase' # imported from a universal variable, see `help abbr`
abbr -a -- gupav 'git pull --rebase --autostash -v' # imported from a universal variable, see `help abbr`
abbr -a -- grb 'git rebase' # imported from a universal variable, see `help abbr`
abbr -a -- gwta 'git worktree add' # imported from a universal variable, see `help abbr`
abbr -a -- gwtmv 'git worktree move' # imported from a universal variable, see `help abbr`
abbr -a -- gc 'git commit -v' # imported from a universal variable, see `help abbr`
abbr -a -- gwtpr 'git worktree prune' # imported from a universal variable, see `help abbr`
abbr -a -- gdca 'git diff --cached' # imported from a universal variable, see `help abbr`
abbr -a -- gwtrm 'git worktree remove' # imported from a universal variable, see `help abbr`
abbr -a -- gmom 'git merge origin/(__git.default_branch)' # imported from a universal variable, see `help abbr`
abbr -a -- gwtulo 'git worktree unlock' # imported from a universal variable, see `help abbr`
#abbr -a -- upall 'yay -Syu --noconfirm' # imported from a universal variable, see `help abbr`
abbr -a -- upall 'yay -Syyu' # imported from a universal variable, see `help abbr`
abbr -a -- update 'sudo pacman -Syyu' # imported from a universal variable, see `help abbr`
abbr -a -- gco 'git checkout' # imported from a universal variable, see `help abbr`
abbr -a -- userlist 'cut -d: -f1 /etc/passwd' # imported from a universal variable, see `help abbr`
abbr -a -- v vim # imported from a universal variable, see `help abbr`
abbr -a -- gd 'git diff' # imported from a universal variable, see `help abbr`
abbr -a -- vmstat 'vmstat -S M 1' # imported from a universal variable, see `help abbr`
abbr -a -- glgga 'git log --graph --decorate --all' # imported from a universal variable, see `help abbr`
#abbr -a -- less less.sh # imported from a universal variable, see `help abbr`
abbr -a -- grv 'git remote -v' # imported from a universal variable, see `help abbr`
abbr -a -- gsta 'git stash' # imported from a universal variable, see `help abbr`
abbr -a -- gfbs 'git flow bugfix start' # imported from a universal variable, see `help abbr`
abbr -a -- gwtls 'git worktree list' # imported from a universal variable, see `help abbr`
abbr -a -- gscam 'git commit -S -a -m' # imported from a universal variable, see `help abbr`
abbr -a -- gsh 'git show' # imported from a universal variable, see `help abbr`
abbr -a -- gstd 'git stash drop' # imported from a universal variable, see `help abbr`
abbr -a -- grbs 'git rebase --skip' # imported from a universal variable, see `help abbr`
abbr -a -- gb 'git branch -vv' # imported from a universal variable, see `help abbr`
abbr -a -- gban 'git branch -a -v --no-merged' # imported from a universal variable, see `help abbr`
abbr -a -- gsur 'git submodule update --recursive' # imported from a universal variable, see `help abbr`
abbr -a -- grst 'git restore --staged' # imported from a universal variable, see `help abbr`
abbr -a -- gdsc 'git diff --stat --cached' # imported from a universal variable, see `help abbr`
abbr -a -- gcp 'git cherry-pick' # imported from a universal variable, see `help abbr`
abbr -a -- gbsr 'git bisect reset' # imported from a universal variable, see `help abbr`
abbr -a -- gll 'git pull origin' # imported from a universal variable, see `help abbr`
abbr -a -- gsw 'git switch' # imported from a universal variable, see `help abbr`
abbr -a -- em 'emacsclient -c -a vim'
abbr -a -- tl 'sudo timeshift --list'
abbr -a -- tc 'sudo timeshift --check'
abbr -a -- img 'nsxiv'

source ~/.config/fish/nnn.fish

#. ~/bash.command-not-found

function fish_command_not_found
  ~/fish-insulter.fish
end

colorscript random
#alsi
zoxide init --cmd c fish | source

