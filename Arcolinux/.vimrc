set nocompatible
set hidden " opening a new file when the current buffer has unsaved changes causes files to be hidden instead of closed
set clipboard=unnamed " Vim will use the clipboard register '*' for all yank, delete, change 
"set number
set number relativenumber
set nowrap "word wraping
set autoindent
set smartindent
set shiftwidth=4 ts=4 et
set softtabstop=4 "Number of spaces that a <Tab> counts for while performing editing operations, like inserting a <Tab> or using <BS>.
set expandtab "In Insert mode: Use the appropriate number of spaces to insert a	<Tab>
set hlsearch "When there is a previous search pattern, highlight all its matches.
set virtualedit=all,block,onemore
set history=1000
set showmatch
set incsearch
set ignorecase
set showcmd
set backspace=indent,eol,start
set nostartofline
set rtp+=~/.fzf
set confirm
set backup
set undofile
set undolevels=200
set backupdir=$HOME/.vimbackup
set visualbell
set t_vb=
set cmdheight=1
set laststatus=2
set ruler
set listchars=tab:\|\ 
set list
set wildmode=longest:list,full
set mouse=a
filetype on
filetype indent on
filetype plugin indent on
scriptencoding utf-8
highlight link RedundantSpaces Error
syntax on  " syntax highlighting
let mapleader=" " "map leader key to space
let g:is_sh = 1 "Set default sh
au BufEnter,BufRead * match RedundantSpaces "\t"
au BufEnter,BufRead * match RedundantSpaces "[[:space:]]\+$"

set cursorline
hi Conceal      term=reverse    cterm=none      ctermfg=237     ctermbg=none
hi Cursorline   term=underline  cterm=bold      ctermfg=none    ctermbg=238
hi NonText      term=reverse    cterm=none      ctermfg=237     ctermbg=none
hi Normal       term=none       cterm=none      ctermfg=252     ctermbg=234
hi SpecialKey   term=reverse    cterm=none      ctermfg=14      ctermbg=23
    
    if &term == "tmux-256color"
        set t_Co=256
    endif
    
    if !has('gui_running') && &term =~ '^\%(screen\|tmux\)'
        " Better mouse support, see  :help 'ttymouse'
        set ttymouse=sgr

        " Enable true colors, see  :help xterm-true-color
        let &termguicolors = v:true
        let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
        let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"

        " Enable bracketed paste mode, see  :help xterm-bracketed-paste
        let &t_BE = "\<Esc>[?2004h"
        let &t_BD = "\<Esc>[?2004l"
        let &t_PS = "\<Esc>[200~"
        let &t_PE = "\<Esc>[201~"

        " Enable focus event tracking, see  :help xterm-focus-event
        let &t_fe = "\<Esc>[?1004h"
        let &t_fd = "\<Esc>[?1004l"
        execute "set <FocusGained>=\<Esc>[I"
        execute "set <FocusLost>=\<Esc>[O"

        " Enable modified arrow keys, see  :help arrow_modifiers
        execute "silent! set <xUp>=\<Esc>[@;*A"
        execute "silent! set <xDown>=\<Esc>[@;*B"
        execute "silent! set <xRight>=\<Esc>[@;*C"
        execute "silent! set <xLeft>=\<Esc>[@;*D"
    endif

    if exists('+termguicolors')
        let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
        let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
        set termguicolors
    endif

"nnoremap ; :
"nnoremap : ;
    nnoremap <silent> <Leader>t :tab split<CR>    
    nnoremap <silent> <Space> :nohlsearch<Bar>:echo<CR>
    nnoremap <Leader>h :history<CR>:history<Space>
    nnoremap <Leader>b :buffers<CR>:buffer<Space>
    nnoremap <silent> <C-b> :Buffers<CR>
    nnoremap <silent> <C-h> :History<CR>
    nnoremap <F2> :<C-U>setlocal lcs=tab:>-,trail:-,eol:$ list! list? <CR>
    "Movement with HJKL in INSERT mode (ALT as a modifier)
    inoremap <A-h> <C-o>h
    inoremap <A-j> <C-o>j
    inoremap <A-k> <C-o>k
    inoremap <A-l> <C-o>l
    inoremap <A-u> <C-o>u
    inoremap <A-y> <C-o>y
    inoremap jj <Esc>

" Start interactive EasyAlign in visual mode (e.g. vipga)
    xmap ga <Plug>(EasyAlign)
" Start interactive EasyAlign for a motion/text object (e.g. gaip)
    nmap ga <Plug>(EasyAlign)

"nnoremap <silent> <C-f> :Files<CR>
"nnoremap <silent> <Leader>f :Rg<CR>
    map n nzz
"map <S-Insert> <C-i>
"Map Y to act like D and C, i.e. to yank until EOL, rather than act as yy, which is the default
    map Y y$                                                           
"Use :w!! to write to a file using sudo
    cmap w!! %!sudo tee > /dev/null %	                               
autocmd BufEnter * execute "chdir ".escape(expand("%:p:h"), ' ')

    map <C-m> :NERDTreeToggle<CR>
    map <S-Right> :tabn<CR>
    map <S-Left>  :tabp<CR>
    map <C-n>     :tabnew<CR>
    map <C-c>     :tabclose<CR>
    nnoremap <C-q> :close<CR>
    nnoremap <silent> <Leader>v :NERDTreeFind<CR>
" Exit Vim if NERDTree is the only window remaining in the only tab.
autocmd BufEnter * if tabpagenr('$') == 1 && winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | quit | endif
let NERDTreeShowHidden=1
let g:NERDTreeIndicatorMapCustom = { "Modified" : "✹", "Staged" : "✚", "Untracked" : "✭", "Renamed" : "➜", "Unmerged" : "═", "Deleted" : "✖", "Dirty" : "✗", "Clean" : "✔", 'Ignored' : '☒', "Unknown" : "?", }
let g:webdevicons_enable = 1
let g:webdevicons_enable_unite = 1
let g:colorizer_auto_color = 1

"Ag: Start ag in the specified directory e.g. :Ag ~/foo 
function! s:ag_in(bang, ...)
    if !isdirectory(a:1)
        throw 'not a valid directory: ' .. a:1
    endif
    " Press `?' to enable preview window.
    call fzf#vim#ag(join(a:000[1:], ' '),
                \ fzf#vim#with_preview({'dir': a:1}, 'right:50%', '?'), a:bang)
endfunction

" Ag call a modified version of Ag where first arg is directory to search
command! -bang -nargs=+ -complete=dir Ag call s:ag_in(<bang>0, <f-args>)

" search for content in files in current project, including hidden ones and ignoring git
function! SearchContentInFiles(query, fullscreen)
  let command_fmt = 'rg --column --line-number --no-heading --color=always --smart-case --hidden -g "!.git" -- %s || true'
  let initial_command = printf(command_fmt, shellescape(a:query))
  let reload_command = printf(command_fmt, '{q}')
  let spec = {'options': ['--phony', '--query', a:query, '--bind', 'change:reload:'.reload_command, '--exact', '--pointer=@', '-i', '-m', '--cycle', '--border=none', '--marker=*', '--ansi', '--preview-window=left,50%', '--bind=alt-bspace:backward-kill-word,ctrl-x:beginning-of-line+kill-line,ctrl-a:select-all', '--color=16,fg+:bright-red,hl:bright-blue,hl+:green,query:blue,prompt:yellow,info:magenta,pointer:bright-yellow,marker:bright-blue,spinner:bright-blue,header:blue', '--prompt=content from files(+hidden) (git ignored) at . > ']}
  let spec = fzf#vim#with_preview(spec, 'right', 'ctrl-/')
  call fzf#vim#grep(initial_command, 1, fzf#vim#with_preview(spec), a:fullscreen)
endfunction

command! -nargs=* -bang SCIF call SearchContentInFiles(<q-args>, <bang>0)
nnoremap <silent> <S-F> :SCIF!<CR>

call plug#begin('~/.vim/plugged')

Plug 'junegunn/vim-easy-align'
"Plug 'https://github.com/junegunn/vim-github-dashboard.git'
"Plug 'SirVer/ultisnips' | Plug 'honza/vim-snippets'
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
"Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'ryanoasis/vim-devicons'
Plug 'mboughaba/i3config.vim',       { 'for' : 'i3config' }
Plug 'dag/vim-fish',                 { 'for' : 'fish' }
"Plug 'maralla/completor.vim', { 'for' : ['fish', 'vim'] }
Plug 'junegunn/vim-peekaboo'
Plug 'itchyny/lightline.vim'
"Plug 'sickill/vim-monokai'
"Plug 'chrisbra/colorizer'
"Plug 'joshdick/onedark.vim'
"Plug 'sheerun/vim-polyglot'
Plug 'sainnhe/everforest'
Plug 'sainnhe/edge'
Plug 'sainnhe/sonokai'
Plug 'francoiscabrol/ranger.vim' "leader +f

call pathogen#infect()

let g:lightline = {
            \ 'colorscheme': 'everforest',
            \ 'active': {
            \   'left': [ [ 'mode', 'paste' ], [ 'readonly', 'absolutepath', 'modified' ] ],
            \ },
            \ }
if &term == 'xterm-kitty'
    let &t_ut=''
endif
if &term == 'rxvt-unicode-256color'
    let &t_ut=''
endif
if &term == 'alacritty'
    let &t_ut=''
endif

call plug#end()

"colorscheme onedark
"colorscheme one
"colorscheme monokai-pro

" important!!
        if has('termguicolors')
          set termguicolors
        endif
" For dark version.
        set background=dark
" For light version.
"       set background=light
" Available values: 'hard', 'medium'(default), 'soft'
        let g:everforest_background = 'hard'
        let g:everforest_better_performance = 1

"The configuration options should be placed before `colorscheme edge`.
"Available values:   `'default'`, `'aura'`, `'neon'`
        let g:edge_style = 'aura'
        let g:edge_better_performance = 1

" The configuration options should be placed before `colorscheme sonokai`.
"Available values:   `'default'`, `'atlantis'`, `'andromeda'`, `'shusia'`, `'maia'`, `'espresso'`
        let g:sonokai_style = 'espresso'
        let g:sonokai_better_performance = 1

"colorscheme sonokai
colorscheme everforest
"colorscheme edge
"colorscheme nord

