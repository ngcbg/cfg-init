# vim:fileencoding=utf-8:foldmethod=marker

[[ $- != *i* ]] && return
[[ $- == *i* ]] && stty -ixon

# Various config files {{{

[[ -f /etc/bash_completion ]] && . /etc/bash_completion
[[ -f /usr/share/bash_completion/bash_completion ]] && . /usr/share/bash_completion/bash_completion 
[[ -f /etc/profile ]] && . /etc/profile
[[ -f ~/.bash_aliases ]] && . /home/ngc/.bash_aliases

#export FZF_DEFAULT_COMMAND="fd . $HOME"
eval "$(fzf --bash)"
[[ -f ~/.fzf.bash ]] && . /home/ngc/.fzf.bash
if type rg &> /dev/null; then
  export FZF_DEFAULT_COMMAND="rg -. --files . $HOME"
  export FZF_DEFAULT_OPTS="-m"
fi

#set -o vi

#if [ -f ~/.bash_aliases ]; then
#    . ~/.bash_aliases
#fi

#}}}

# Define Colors {{{
red='\[\e[0;31m\]'
RED='\e[1;31m'
blue='\e[0;34m'
BLUE='\e[1;34m'
cyan='\e[0;36m'
CYAN='\e[1;36m'
NC='\e[0m' # No Color
black='\e[0;30m'
BLACK='\e[1;30m'
green='\e[0;32m'
GREEN='\e[1;32m'
yellow='\e[0;33m'
YELLOW='\e[1;33m'
magenta='\e[0;35m'
MAGENTA='\\e[1;35m'
white='\e[0;37m'
WHITE='\e[1;37m'
# }}}

# Some one-liners: have(), lsd() {{{
have() { type "$1" &> /dev/null; }
function lsd(){ ls -l "$@"| egrep "^d" ; ls -lXB "$@" 2>&-| egrep -v "^d|total "; }
# }}}

# fstr() -- Find string in files {{{
function fstr()
{
 OPTIND=1
 local case=""
 local usage="fstr: find string in files."
 #Usage: fstr [-i] \"pattern\" [\"filename pattern\"] "
	while getopts :it opt
	do
		case "$opt" in
			i) case="-i " ;;
			*) echo "$usage"; return;;
		esac
	done
 shift $(( $OPTIND - 1 ))
	if [ "$#" -lt 1 ]; then
		echo "$usage"
		return;
	fi
 local SMSO=$(tput smso)
 local RMSO=$(tput rmso)
# find . -type f -name "${2:-*}" -print0 | xargs -0 grep -sn ${case} "$1" 2>&- | sed "s/$1/${SMSO}\0${RMSO}/gI" | more
 find . -type f -name $1
}
# }}}

# swap() -- switch 2 filenames around {{{
function swap()
{
    local TMPFILE=tmp.$$
    mv "$1" $TMPFILE
    mv "$2" "$1"
    mv $TMPFILE "$2"
}
# }}}

# ni() -- networking information {{{
function ni() # get current host related info
{
echo -e "\nYou are logged on ${RED}$HOST"
    echo -e "\nAdditionnal information:$NC " ; uname -a
    echo -e "\n${RED}Users logged on:$NC " ; w -h
    echo -e "\n${RED}Current date :$NC " ; date
    echo -e "\n${RED}Machine stats :$NC " ; uptime
    echo -e "\n${RED}Memory stats :$NC " ; free
    MY_IP=$(/sbin/ifconfig enp8s0 | awk '/inet/ { print $2 } ' | sed -e s/addr://)
    MY_IP_WIFI=$(/sbin/ifconfig wlan0 | awk '/inet/ { print $2 } ' | sed -e s/addr://)
    echo -e "\n${RED}Local IP Address :$NC" ; echo ${MY_IP:-"Not connected"}
    echo -e "\n${RED}Local IP Address (Wireless) :$NC" ; echo ${MY_IP_WIFI:-"Not connected"}
    echo
}
# }}}

# repeat() -- repeat a given command N times {{{
function repeat() # repeat n times command
{
    local i max
    max=$1; shift;
    for ((i=1; i <= max ; i++)); do
eval "$@";
    done
}
# }}}

# ask() -- ask user a yes/no question {{{
function ask()
{
    echo -n "$@" '[y/N] ' ; read ans
    case "$ans" in
        y*|Y*) return 0 ;;
        *) return 1 ;;
    esac
}
# }}}

# f() {{{

# Run command/application and choose paths/files with fzf.
# Always return control of the terminal to user (e.g. when opening GUIs).
# The full command that was used will appear in your history just like any
# other (N.B. to achieve this I write the shell's active history to
# ~/.bash_history)
#
# Usage:
# f cd [OPTION]... (hit enter, choose path)
# f cat [OPTION]... (hit enter, choose files)
# f vim [OPTION]... (hit enter, choose files)
# f vlc [OPTION]... (hit enter, choose files)

f() {
    # if no arguments passed, just lauch fzf
    if [ $# -eq 0 ]
    then
        fzf | sort
        return 0
    fi

    # Store the program
    program="$1"

    # Remove first argument off the list
    shift

    # Store any option flags
    options="$@"

    # Store the arguments from fzf
    arguments=$(fzf --multi)

    # If no arguments passed (e.g. if Esc pressed), return to terminal
    if [ -z "${arguments}" ]; then
        return 1
    fi

    # Sanitise the command by putting single quotes around each argument, also
    # first put an extra single quote next to any pre-existing single quotes in
    # the raw argument. Put them all on one line.
    for arg in "${arguments[@]}"; do
        arguments=$(echo "$arg" | sed "s/'/''/g; s/.*/'&'/g; s/\n//g")
    done

    # If the program is on the GUI list, add a '&'
    if [[ "$program" =~ ^(nautilus|zathura|evince|vlc|eog|kolourpaint)$ ]]; then
        arguments="$arguments &"
    fi

    # Write the shell's active history to ~/.bash_history.
    history -w

    # Add the command with the sanitised arguments to .bash_history
    echo $program $options $arguments >> ~/.bash_history

    # Reload the ~/.bash_history into the shell's active history
    history -r

    # execute the last command in history
    fc -s -1
    }
# }}}

# search through directory contents with grep' {{{
function lsgrep() {
	ls | grep "$@"
}
# }}}

# # ex = EXtractor for all kinds of archives {{{
# # usage: ex <file>
ex ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1   ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *.deb)       ar x $1      ;;
      *.tar.xz)    tar xf $1    ;;
      *.tar.zst)   tar xf $1    ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

# }}}

# rot13() {{{
rot13 () { 
    if [ $# -eq 0 ]; then
tr '[a-m][n-z][A-M][N-Z]' '[n-z][a-m][N-Z][A-M]'
    else
echo $* | tr '[a-m][n-z][A-M][N-Z]' '[n-z][a-m][N-Z][A-M]'
    fi
}
# }}}

# Un-rot13 {{{
unrot13 () {
	if [ $# -eq 0 ]; then
tr 'N-ZA-Mn-za-m5-90-4' 'A-Za-z0-9'
	else
echo $* | tr 'N-ZA-Mn-za-m5-90-4' 'A-Za-z0-9'
	fi
}
# }}}

# isprime() -- Is $1 prime? {{{
isprime () {
    perl -wle 'print "Prime" if (1 x shift) !~ /^1?$|^(11+?)\1+$/'
#.~/.isprime
}
# }}}

# Display 256 colors {{{
function aa_256 () 
{ 
    local o=i=x=`tput op` cols=`tput cols` y=oo=yy=;
    y=`printf %$(($cols-6))s`;
    yy=${y// /=};
    for i in {0..256};
    do
        o=00${i};
        oo=`echo -en "setaf ${i}\nsetab ${i}\n"|tput -S`;
        echo -e "${o:${#o}-3:3} ${oo}${yy}${x}";
    done
}
# }}}

# Display 256 colors II {{{
function aa_c666 ()
{ 
    local r= g= b= c= CR="`tput sgr0;tput init`" C="`tput op`" n="\n\n\n" t="  " s="    ";
    echo -e "${CR}${n}";
    function c666 () 
    { 
        local b= g=$1 r=$2;
        for ((b=0; b<6; b++))
        do
            c=$(( 16 + ($r*36) + ($g*6) + $b ));
            echo -en "setaf ${c}\nsetab ${c}\n" | tput -S;
            echo -en "${s}";
        done
    };
    function c666b () 
    { 
        local g=$1 r=;
        for ((r=0; r<6; r++))
        do
            echo -en " `c666 $g $r`${C} ";
        done
    };
    for ((g=0; g<6; g++))
    do
        c666b=`c666b $g`;
        echo -e " ${c666b}";
        echo -e " ${c666b}";
        echo -e " ${c666b}";
        echo -e " ${c666b}";
        echo -e " ${c666b}";
    done;
    echo -e "${CR}${n}${n}"
}
# }}}

# Print terminal colors etc. {{{
c ()
{
    tput clear;
    pm "$TERM: [colors:`tput colors`/`tput pairs`]";
    RC=`tput op` L1=$(L '=' $(( ${COLUMNS} - 25 )));
    for i in `seq ${1:-0} ${2:-16}`;
    do
        o="  $i";
        echo -e " ${o:${#o}-3:3} `tput setaf $i;tput setab $i`${L1}${RC}";
    done
}
# }}}

# Search engine - fzf+ripgrep(rg) {{{
fif() {
  if [ ! "$#" -gt 0 ]; then echo "Need a string to search for!"; return 1; fi
  rg --files-with-matches --no-messages "$1" | fzf --preview "highlight -O ansi -l {} 2> /dev/null | rg --colors 'match:bg:yellow' --ignore-case --pretty --context 10 '$1' || rg --ignore-case --pretty --context 10 '$1' {}"
}
#}}}

# Man with fzf {{{
fman() {
    man -k . | fzf -q "$1" --prompt='man> '  --preview $'echo {} | tr -d \'()\' | awk \'{printf "%s ", $2} {print $1}\' | xargs -r man | col -bx | bat -l man -p --color always' | tr -d '()' | awk '{printf "%s ", $2} {print $1}' | xargs -r man -P most
}
#}}}

le() {
  pygmentize -P style=nord "$1" | /usr/bin/less -iMFNceqwRSX -x2 --use-color
}

export HISTCONTROL=ignoreboth:erasedups
export GTK_OVERLAY_SCROLLING=0
export EDITOR='nvim'
export VISUAL='nvim'
export BROWSER='/usr/bin/firefox'
export TERMINAL='kitty'
export QT_AUTO_SCREEN_SCALE_FACTOR=1
export QT_SCALE_FACTOR=1

PS1='[\u@\h \W]\$ '

if [ -d "$HOME/.bin" ] ;
  then PATH="$HOME/.bin:$PATH"
fi

if [ -d "$HOME/.local/bin" ] ;
  then PATH="$HOME/.local/bin:$PATH"
fi

if [ -t 1 ]; then
    bind "set completion-ignore-case on"
    bind "TAB:menu-complete"
    bind "set show-all-if-ambiguous on"
    bind "set menu-complete-display-prefix on"
    bind '""\e[Z":menu-complete-backward'
fi

# Set some bash options {{{
set -o notify
shopt -s checkhash
shopt -s checkwinsize
shopt -s sourcepath
shopt -s no_empty_cmd_completion
shopt -s cmdhist
shopt -s autocd # change to named directory
shopt -s cdspell # autocorrects cd misspellings
shopt -s cmdhist # save multi-line commands in history as single line
shopt -s dotglob
shopt -s nocaseglob
shopt -s globstar
shopt -s histappend histreedit histverify # do not overwrite history
export HISTFILESIZE=500000
export HISTSIZE=100000
export HISTIGNORE='&:cd:ls:bin/ss;history *'
bind '"\e[B": history-search-forward'
bind '"\e[A": history-search-backward'
export HISTCONTROL='ignoreboth'
shopt -s expand_aliases # expand aliases
# }}}

# Powerline
powerline-daemon -q
source /usr/share/powerline/bindings/bash/powerline.sh

if [ -f ~/bash.command-not-found ]; then
    . /home/ngc/bash.command-not-found
fi

#neofetch
ufetch
#curl ascii.live/can-you-hear-me
eval "$(zoxide init --cmd c bash)"
