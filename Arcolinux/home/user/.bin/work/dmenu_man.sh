#!/usr/bin/bash 
#set -x MANPAGER sh -c 'col -bx | bat -l man -p'
man -k -S 1 . | dmenu -fn 'IBM Plex Mono:pixelsize=18' -l 30 | awk '{print $1}' | xargs -r xterm -e man -P less
