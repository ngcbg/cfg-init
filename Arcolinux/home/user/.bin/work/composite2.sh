#!/bin/bash

#This script is designed to provide a user-friendly menu to control and
#manage compositors on a UNIX/Linux system, specifically Picom and Xcompmgr,
#both of which are compositing window managers used for graphical effects
#such as transparency and shadows. The script uses rofi (a window switcher,
#run launcher, and dmenu replacement) to present options to the user and
#notify-send to give notifications about the actions taken.

# ngc 2023; last rev. 2024

# Function to display notifications
notify_user() {
    local message=$1
    notify-send -t 3000 "$message"
}

# Function to check if Picom is running
is_picom_running() {
    pgrep -x picom &>/dev/null
}

# Function to check if Xcompmgr is running
is_xcompmgr_running() {
    pgrep -x xcompmgr &>/dev/null
}

# Function to start Picom
start_picom() {
    # If Xcompmgr is running, stop it before starting Picom
    if is_xcompmgr_running; then
        killall xcompmgr
    fi
    # Start Picom
    /usr/bin/picom -b --vsync-use-glfinish --config ~/picom.NGC2 2>&1 &
    notify_user "Turning ON Picom "
}

# Function to stop Picom
stop_picom() {
    killall picom
    notify_user "DEACTIVATING Picom... "
}

# Function to start Xcompmgr
start_xcompmgr() {
    # If Picom is running, stop it before starting Xcompmgr
    if is_picom_running; then
        killall picom
    fi
    # Start Xcompmgr
    /usr/bin/xcompmgr -c -C -f -F -r4.2 -o.40 -D3 2>&1 &
    notify_user "Starting Xcompmgr... "
}

# Function to stop Xcompmgr
stop_xcompmgr() {
    killall xcompmgr
    notify_user "DEACTIVATING Xcompmgr... "
}

# Function to restart the current composite manager
restart_compositor() {
    if is_xcompmgr_running; then
        stop_xcompmgr
        start_xcompmgr
        notify_user "Xcompmgr has been restarted"
    elif is_picom_running; then
        stop_picom
        start_picom
        notify_user "Picom has been restarted "
    else
        notify_user "No active composite manager"
    fi
}

# Function to determine which composite manager is running
check_compositor() {
    if is_xcompmgr_running; then
        notify_user "Xcompmgr is ACTIVE"
    elif is_picom_running; then
        notify_user "Picom is ACTIVE"
    else
        notify_user "No active composite manager"
    fi
}

# Function to stop all composite managers
stop_all_compositors() {
    if is_xcompmgr_running; then
        stop_xcompmgr
    elif is_picom_running; then
        stop_picom
    fi
    notify_user "No active composite manager"
}

# Rofi menu function
RofiMenu() {
    # Menu options
    options="🔵  Start Picom\n❌ Stop Picom\n🔵  Start Xcompmgr\n❌ Stop Xcompmgr\n⚡ Restart current composite manager\n❓ Which composite manager is running?\n⭕ Stop composite manager"

    # Display options via Rofi
    choice=$(printf '%b' "$options" | rofi -config ~/.config/rofi/nord.rasi -dmenu)

    # Handle user selection
    case "$choice" in
        "🔵  Start Picom")
            if is_picom_running; then
                notify_user "Picom is ACTIVE "
            else
                start_picom
            fi
            ;;
        "❌ Stop Picom")
            if is_picom_running; then
                stop_picom
            else
                notify_user "Picom is NOT active "
            fi
            ;;
        "🔵  Start Xcompmgr")
            if is_xcompmgr_running; then
                notify_user "Xcompmgr is ACTIVE "
            else
                start_xcompmgr
            fi
            ;;
        "❌ Stop Xcompmgr")
            if is_xcompmgr_running; then
                stop_xcompmgr
            else
                notify_user "Xcompmgr is NOT active "
            fi
            ;;
        "⚡ Restart current composite manager")
            restart_compositor
            ;;
        "❓ Which composite manager is running?")
            check_compositor
            ;;
        "⭕ Stop composite manager")
            stop_all_compositors
            ;;
    esac
}

# Main script logic
case "$1" in
    "--rofi-menu")
        RofiMenu
        ;;
esac
