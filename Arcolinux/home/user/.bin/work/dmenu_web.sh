#!/usr/bin/env bash
#
# This script provides a quick interface to search via different search engines or websites using dmenu.
# The user selects a search engine, inputs a query, and the search is opened in the browser.

# initializeANSI()
# {
#   esc=""
#
# redf="${esc}[31m"   
# yellowf="${esc}[33m" 
# cyanf="${esc}[36m"  
# greenf="${esc}[32m"
# boldon="${esc}[1m"
# reset="${esc}[0m"
# bluef="${esc}[34m"   
# purplef="${esc}[35m"  
# }

#BROWSER="luakit"
#BROWSER="chromium"
BROWSER="qutebrowser"
#BROWSER="vimb"
#BROWSER="firefox"

# Check if the specified browser is available
if ! command -v "$BROWSER" &> /dev/null; then
    echo "Error: Browser '$BROWSER' is not installed or available in the PATH."
    exit 1
fi
#initializeANSI

declare -a options=(
"startpage - https://www.startpage.com/sp/search?q="
"brave - https://search.brave.com/search?q="
"searx - https://searx.tiekoetter.com/search?q="
"duckduckgo - https://www.duckduckgo.com/?q="
"bing - https://www.bing.com/search?q="
"amazon - https://www.amazon.com/s?url=search-alias%3Daps&field-keywords="
"maps - https://www.google.com/maps/search/"
"ebay - https://www.ebay.com/sch/"
"wikipedia - https://en.wikipedia.org/wiki/Special:Search?search="
"steam - https://store.steampowered.com/search/?term="
"youtube - https://www.youtube.com/results?search_query="
"imdb - http://www.imdb.com/find?ref_=nv_sr_fn&q="
"github - https://github.com/search?q="
"stackoverflow - http://stackoverflow.com/search?q="
"searchcode - https://searchcode.com/?q="
"openhub - https://www.openhub.net/p?ref=homepage&query="
"superuser - http://superuser.com/search?q="
"vimawesome - http://vimawesome.com/?q="
#  ["piratebay"]="https://thepiratebay.org/search/"
#  ["goodreads"]="https://www.goodreads.com/search?q="
#  ["yahoo"]="https://search.yahoo.com/search?p="
#  ["askubuntu"]="http://askubuntu.com/search?q="
#  ["rottentomatoes"]="https://www.rottentomatoes.com/search/?search="
#  ["yandex"]="https://yandex.ru/yandsearch?text="
#  ["symbolhound"]="http://symbolhound.com/?q="
"quit"
)

choice=$(printf '%s\n' "${options[@]}" | dmenu -fn 'IBM Plex Mono:pixelsize=18' -i -l 20 -p 'Search via:')

# For testing purposes
#echo "$choice"

# If the user chooses "quit", exit the program
if [[ "$choice" == "quit" ]]; then
    echo "Program quit!" && exit 0
fi

uri=$(printf '%s\n' "${choice}" | awk '{print $3}')
    if [[ -n "$uri" ]]; then
         query=$( echo '' | dmenu -fn 'IBM Plex Mono:pixelsize=18' -i -p "Query > " )

       if [[ -n "$query" ]]; then
            url=${uri}$query

        $BROWSER "$url" 2> /dev/null

        else
            echo "No query entered. Exiting."
            exit 1
        fi
     fi
