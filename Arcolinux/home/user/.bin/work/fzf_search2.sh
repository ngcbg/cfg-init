#!/usr/bin/env bash

# Initialize the search using fzf with fd as the file search command.
# Limiting the search to specific directories for better performance.
export FZF_DEFAULT_COMMAND="fd --type f . \
  $HOME/* \
  $HOME/.bin \
  $HOME/Documents \
  $HOME/Music \
  $HOME/Downloads \
  $HOME/.config \
  $HOME/.local \
  "

# Use fzf with multiple options for preview, cycling, and layout customization.
selected="$(fzf -i --delimiter / \
  --preview 'bat --color=always {}'  \
  --multi  --cycle --keep-right -1 \
  --layout=reverse  --info=default \
  --border=double \
  --info=inline \
  --prompt='$>' \
  --pointer='→' \
  --marker='♡' \
  --header='CTRL-c or ESC to quit' \
  --color='dark,fg:magenta'
)"

# Trap the HUP signal to prevent the script from being killed on terminal disconnect.
trap "" HUP

# Error handling: exit if no file was selected.
if [[ -z "$selected" ]]; then
  echo "No file selected. Exiting."
  exit 1
fi

# If "dir" is passed as the first argument, open with Thunar and copy the path to the clipboard.
if [[ $1 == "dir" ]]; then
  printf "%s\n" "${selected}" | xclip -selection clipboard >/dev/null 2>&1 &
  thunar "$selected" >/dev/null 2>&1 &
else
  # Otherwise, open the selected file with xdg-open.
  xdg-open "$selected" >/dev/null 2>&1 &
fi

