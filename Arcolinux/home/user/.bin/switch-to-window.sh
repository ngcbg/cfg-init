#!/bin/bash

generate_window_list() {
    wmctrl -xl | while read -r window_id desktop class hostname title; do
        printf "%-15s %-30s %s\n" "$window_id" "${class##*.}" "$title"
    done | sort -k 2
}

case "$1" in
    "")
        prompt="Go to window:"
        action() { wmctrl -ia "$window_id"; }
        ;;
    "--bring")
        prompt="Bring window:"
        action() { i3-msg "[id=$window_id] move to workspace current, focus"; }
        ;;
    *)
        echo "Unknown option $1" >&2
        exit 1
        ;;
esac

option="$(generate_window_list | dmenu -p "$prompt" -i -l 10)" || exit 1
window_id="${option%% *}"
action
