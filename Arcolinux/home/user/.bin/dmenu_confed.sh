#!/usr/bin/env bash
#
DMEDITOR="kitty -e nvim"

declare -a options=(
"alacritty - $HOME/.config/alacritty/alacritty.toml"
"kitty - $HOME/.config/kitty/kitty.conf"
"i3status-rs - $HOME/.config/i3status-rust/config.toml"
"picom - $HOME/picom.NGC2"
"i3 Main - $HOME/.config/i3/config"
"i3 Bar -  $HOME/.config/i3/bar.conf"
"i3 Floating -  $HOME/.config/i3/floating.conf"
"i3 Gaps -  $HOME/.config/i3/gaps.conf"
"i3 Modes -  $HOME/.config/i3/modes.conf"
"i3 Movement -  $HOME/.config/i3/movement.conf"
"i3 Shortcuts -  $HOME/.config/i3/shortcuts.conf"
"i3 Startup -  $HOME/.config/i3/startup.conf"
"i3 Workspaces -  $HOME/.config/i3/workspaces.conf"
"Xresources - $HOME/.Xresources"
"dunst - $HOME/.config/dunst/dunstrc"
"fish - $HOME/.config/fish/config.fish"
"bashrc - $HOME/.bashrc"
"bash_aliases - $HOME/.bash_aliases"
"cwmrc - $HOME/.cwmrc"
"cwm autostart - $HOME/./config/cwm/autostart.sh"
"tmux - $HOME/.config/tmux/.tmux.conf"
"zshrc - $HOME/.zshrc"
"zsh_aliasrc - $HOME/.zsh_alaisrc"
"vimrc - $HOME/.vimrc"
"quit"
)

choice=$(printf '%s\n' "${options[@]}" | dmenu -fn 'IBM Plex Mono:pixelsize=18' -i -l 20 -p 'Edit config:')
echo $choice

    if [[ "$choice" == "quit" ]]; then
        echo "Program terminated!" && exit 1

    elif [ "$choice" ]; then
        cfg=$(printf '%s\n' "${choice}" | awk '{print $NF}')
        $DMEDITOR "$cfg"

    else
        echo "Program quit!" && exit 1
    fi

