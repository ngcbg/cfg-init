import i3ipc
import subprocess
import os

# Define the configuration file path
CONFIG_FILE = os.path.expanduser('~/.thunar_state')

def is_program_running(window_class):
    for window in i3.get_tree().leaves():
        if window.window_class == window_class:
            return True
    return False

def open_named_workspace_with_program(name, window_class, command):
    # Check if the program is running
    if not is_program_running(window_class):
        # Launch the program
        subprocess.Popen(command)

    # Open a new named workspace
    i3.command(f'workspace {name}; exec {command[0]}')

    # If the workspace is "Thunar", retrieve and restore the state
    if name == 'Thunar':
        state = load_thunar_state()
        if state:
            directory, cursor_position = state
            i3.command(f'exec thunar --directory "{directory}" --view-type "ThunarViewModeIconView"')
            i3.command(f'exec xdotool search --class "Thunar" windowfocus')
            i3.command(f'exec xdotool key --window "$(xdotool getwindowfocus)" --delay 50 ctrl+{cursor_position}')

def load_thunar_state():
    if os.path.exists(CONFIG_FILE):
        with open(CONFIG_FILE, 'r') as f:
            directory, cursor_position = f.read().strip().split(',')
            return directory, cursor_position
    return None

def save_thunar_state(directory, cursor_position):
    with open(CONFIG_FILE, 'w') as f:
        f.write(f'{directory},{cursor_position}')

def on_keybinding_event(i3, e):
    if e.binding.command == 'exec $mod+t':
        workspace_name = 'Thunar'
        window_class = 'Thunar'
        program_command = ['thunar']

        open_named_workspace_with_program(workspace_name, window_class, program_command)

    elif e.binding.command == 'exec $mod+p':
        workspace_name = 'PCmanFM'
        window_class = 'pcmanfm'
        program_command = ['pcmanfm']

        open_named_workspace_with_program(workspace_name, window_class, program_command)

    elif e.binding.command == 'exec $mod+k':
        workspace_name = 'Kitty'
        window_class = 'kitty'
        program_command = ['kitty']

        open_named_workspace_with_program(workspace_name, window_class, program_command)

    elif e.binding.command == 'exec $mod+Shift+q':
        # When closing Thunar, save the state
        if e.binding.symbol == 'q':
            save_thunar_state('path/to/directory', 'position')

i3 = i3ipc.Connection()
i3.on('binding', on_keybinding_event)
i3.main()

