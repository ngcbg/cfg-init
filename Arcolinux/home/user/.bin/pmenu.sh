#!/bin/sh

<<EOF /usr/local/bin/pmenu -e
Apps
	Firefox	exec /usr/bin/firefox
	Chrome	chromium
	Vivaldi	vivaldi
	Gimp	gimp
Terms
	xterm	xterm
	urxvt	urxvt
	st	st
Halt	poweroff
Reboot	reboot
EOF
