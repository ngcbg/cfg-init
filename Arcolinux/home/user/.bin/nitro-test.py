#!/bin/python

import os
import random
import time


def crawl_directory(directory_path):
    file_list = []
    for root, dirs, files in os.walk(directory_path):
        for file in files:
            file_list.append(os.path.join(root, file))
    return file_list


def choose_random_elements(file_list, chosen_elements):
    random_elements = random.sample(file_list, 2)
    chosen_elements.extend(random_elements)
    for element in random_elements:
        file_list.remove(element)
    return random_elements


def write_to_file(chosen_elements, output_file):
    with open(output_file, 'a') as f:
        for idx, element in enumerate(chosen_elements, start=1):
            f.write(f"{idx}: {element}\n")


def substitute_filenames_with_indexes(chosen_elements):
    index_mapping = {}
    for idx, element in enumerate(chosen_elements, start=1):
        index_mapping[idx] = element
    return index_mapping


def main():
    directory1 = "/home/ngc/Pictures/Wallpaper/new/"
    directory2 = "/home/ngc/Pictures/Wallpaper/nord-background-main/"
    output_file = "/home/ngc/selected_files.txt"

    while True:
        file_list = crawl_directory(directory1) + crawl_directory(directory2)
        chosen_elements = []
        random_elements = choose_random_elements(file_list, chosen_elements)

        index_mapping = substitute_filenames_with_indexes(random_elements)

        # Do something with the index_mapping, for now, let's print it
        print("Index Mapping:", index_mapping)

        write_to_file(random_elements, output_file)

        time.sleep(2)  # Sleep for 15 minutes (900 seconds)


if __name__ == "__main__":
    main()
