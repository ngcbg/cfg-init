#!/usr/bin/env bash

# -----------------------------------------------------------------------------
# Info:
#   author:    Miroslav Vidovic
#   file:      web-search.sh
#   created:   24.02.2017.-08:59:54
#   revision:  ---
#   version:   1.0
# -----------------------------------------------------------------------------
# Requirements:
#   rofi
# Description:
#   Use rofi to search the web.
# Usage:
#   web-search.sh
# -----------------------------------------------------------------------------
# Script:

declare -A URLS

URLS=(
  ["0 startpage"]="https://www.startpage.com/sp/search?q="
  ["1 brave"]="https://search.brave.com/search?q="
  ["2 searx"]="https://searx.tiekoetter.com/search?q="
  ["3 duckduckgo"]="https://www.duckduckgo.com/?q="
  ["4 bing"]="https://www.bing.com/search?q="
  ["5 amazon"]="https://www.amazon.com/s?url=search-alias%3Daps&field-keywords="
  ["6 maps"]="https://www.google.com/maps/search/"
  ["7 ebay"]="https://www.ebay.com/sch/"
  ["8 wikipedia"]="https://en.wikipedia.org/wiki/Special:Search?search="
  ["9 steam"]="https://store.steampowered.com/search/?term="
  ["10 youtube"]="https://www.youtube.com/results?search_query="
  ["11 imdb"]="http://www.imdb.com/find?ref_=nv_sr_fn&q="
  ["12 github"]="https://github.com/search?q="
  ["13 stackoverflow"]="http://stackoverflow.com/search?q="
  ["14 searchcode"]="https://searchcode.com/?q="
  ["15 openhub"]="https://www.openhub.net/p?ref=homepage&query="
  ["16 superuser"]="http://superuser.com/search?q="
  ["17 vimawesome"]="http://vimawesome.com/?q="

#  ["piratebay"]="https://thepiratebay.org/search/"
#  ["goodreads"]="https://www.goodreads.com/search?q="
#  ["yahoo"]="https://search.yahoo.com/search?p="
#  ["askubuntu"]="http://askubuntu.com/search?q="
#  ["rottentomatoes"]="https://www.rottentomatoes.com/search/?search="
#  ["yandex"]="https://yandex.ru/yandsearch?text="
#  ["symbolhound"]="http://symbolhound.com/?q="
)

# List for rofi
gen_list() {
  for i in "${!URLS[@]}"
    do
      echo "$i"
    done  | sort -n
}

main() {
  # Pass the list to rofi
  platform=$( (gen_list) | rofi -theme nord -dmenu -matching fuzzy -no-custom -location 0 -p "Search > " )

  if [[ -n "$platform" ]]; then
    query=$( (echo ) | rofi -theme nord -dmenu -matching fuzzy -location 0 -p "Query > " )

    if [[ -n "$query" ]]; then
      url=${URLS[$platform]}$query
      xdg-open "$url"
    else
      exit
    fi

  else
    exit
  fi
}

main

exit 0
