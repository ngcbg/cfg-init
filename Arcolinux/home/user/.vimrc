if &shell =~# 'fish$'
    set shell=sh
endif

if has('unnamedplus')
  set clipboard=unnamed,unnamedplus
endif

set listchars=tab:>.,trail:.,extends:#,nbsp:. " Highlight problematic whitespace
set nocompatible
set hidden " opening a new file when the current buffer has unsaved changes causes files to be hidden instead of closed
"set clipboard=unnamed " Vim will use the clipboard register '*' for all yank, delete, change 
set autochdir
"set number
set number relativenumber
set nowrap "word wraping
set autoindent
set smartindent
set shiftwidth=4 ts=4 et
set softtabstop=4 "Number of spaces that a <Tab> counts for while performing editing operations, like inserting a <Tab> or using <BS>.
set expandtab "In Insert mode: Use the appropriate number of spaces to insert a	<Tab>
set hlsearch "When there is a previous search pattern, highlight all its matches.
"set virtualedit=all,block,onemore
set history=1000
set showmatch
set incsearch
set ignorecase
set showcmd
set backspace=indent,eol,start " allow backspacing over everything.
set nostartofline " Maintain the horizontal cursor position when scrolling
set rtp+=~/.fzf
set confirm
set backup
set undofile
set undolevels=200
set backupdir=$HOME/.vimbackup
set visualbell
set nostartofline " Make j/k respect the columns
set t_vb= " Disable beeping
set cmdheight=1
set laststatus=2
set ruler
"set listchars=tab:\|\
set diffopt=filler          " Add vertical spaces to keep right and left aligned.
set diffopt+=iwhite         " Ignore whitespace changes.
"set list
set wildmenu                " Hitting TAB in command mode will
set wildchar=<TAB>          "   show possible completions.
set wildmode=longest:list,full
set wildignore+=*.DS_STORE,*.db
set mouse=a
filetype on
filetype indent on
filetype plugin indent on
scriptencoding=UTF-8
set encoding=UTF-8
syntax on  " syntax highlighting
highlight link RedundantSpaces Error
au BufEnter,BufRead * match RedundantSpaces "\t"
au BufEnter,BufRead * match RedundantSpaces "[[:space:]]\+$"

let mapleader=" " "map leader key to space
let NERDTreeShowHidden=1
let g:NERDTreeIndicatorMapCustom = { "Modified" : "✹", "Staged" : "✚", "Untracked" : "✭", "Renamed" : "➜", "Unmerged" : "═", "Deleted" : "✖", "Dirty" : "✗", "Clean" : "✔", 'Ignored' : '☒', "Unknown" : "?", }
let g:webdevicons_enable = 1
let g:webdevicons_enable_unite = 1
"let g:colorizer_auto_color = 1
"let g:colorizer_colornames = 0
let g:is_sh = 1 "Set default sh
" Indentation guide 
let g:indentLine_setColors = 0
let g:indentLine_char_list = ['|', '¦', '┆', '┊']
let g:indentLine_concealcursor = 'inc'
let g:indentLine_conceallevel = 2

" KEY MAPPING
    "nnoremap ; :
    "nnoremap : ;
    nnoremap <silent> <Leader>t :tab split<CR>    
" Disable highlights when you press <leader>
    nnoremap <silent> <Space> :nohlsearch<Bar>:echo<CR>
    nnoremap <Leader>h :history<CR>:history<Space>
    nnoremap <Leader>b :buffers<CR>:buffer<Space>
    nnoremap <silent> <C-b> :Buffers<CR>
    nnoremap <silent> <C-h> :History<CR>
" Show trailing spaces
    nnoremap <F2> :<C-U>setlocal lcs=tab:>-,trail:-,eol:$ list! list? <CR>
"Movement with HJKL in INSERT mode (ALT as a modifier)
    inoremap <A-h> <C-o>h
    inoremap <A-j> <C-o>j
    inoremap <A-k> <C-o>k
    inoremap <A-l> <C-o>l
    inoremap <A-u> <C-o>u
    inoremap <A-y> <C-o>y
    inoremap jj <Esc>
" Fast saving of a buffer (<leader>w):
    nmap <leader>w :w!<cr>
" Smart way to move between windows (<ctrl>j etc.):
    map <S-j> <C-W>j
    map <S-k> <C-W>k
    map <S-h> <C-W>h
    map <S-l> <C-W>l
" Close current buffer
    map <leader>bd :bdelete<cr>
" Close all buffers
    map <leader>ba :1,1000 bd!<cr>
" Switch CWD to the directory of the open buffer:
    map <leader>cd :cd %:p:h<cr>:pwd<cr>
" Path completion with custom source command
    inoremap <expr> <c-x><c-g> fzf#vim#complete#path('rg --files')
    nnoremap <silent> <C-f> :Files<CR>
    nnoremap <silent> <Leader>d :Rg<CR>
" Search results appear in the screen centre
    map n nzz
    map N Nzz
    map <C-u> <C-u>zz
    map <C-d> <C-d>zz
"Map Y to act like D and C, i.e. to yank until EOL, rather than act as yy, which is the default
    map Y y$
"Use :w!! to write to a file using sudo
    cmap w!! %!sudo tee > /dev/null %
" Tabs and NerdTree
    map <C-n> :NERDTreeToggle<CR>
    nnoremap <silent> <Leader>v :NERDTreeFind<CR>
    map <S-Right>  :tabn<CR>
    map <S-Left>   :tabp<CR>
    map <leader>nt :tabnew<CR>
    map <C-c>      :tabclose<CR>
    nnoremap <C-q> :close<CR>
    nnoremap <S-Q> :set number!<CR>:call ToggleSignColumn()<CR>
" Bubble single lines
    nmap <A-Up> ddkP
    nmap <A-Down> ddp
" Bubble multiple lines
    vmap <A-Up> xkP`[V`]
    vmap <A-Down> xp`[V`]

   autocmd BufEnter * execute "chdir ".escape(expand("%:p:h"), ' ')

" Source the vimrc file after saving it
if has("autocmd")
  autocmd bufwritepost .vimrc source $MYVIMRC
endif

"Ag: Start ag in the specified directory e.g. :Ag ~/foo 
    function! s:ag_in(bang, ...)
        if !isdirectory(a:1)
            throw 'not a valid directory: ' .. a:1
        endif
        " Press `?' to enable preview window.
        call fzf#vim#ag(join(a:000[1:], ' '),
                    \ fzf#vim#with_preview({'dir': a:1}, 'right:50%', '?'), a:bang)
    endfunction
" ag command suffix, [options] 

" Ag call a modified version of Ag where first arg is directory to search
    command! -bang -nargs=+ -complete=dir Ag call s:ag_in(<bang>0, <f-args>)
    command! -bang -nargs=+ -complete=dir Rag call fzf#vim#ag_raw(<q-args>, {'options': '--delimiter : --nth 4..'}, <bang>0)

" search for content in files in current project, including hidden ones and ignoring git
    function! SearchContentInFiles(query, fullscreen)
      let command_fmt = 'rg --column --line-number --no-heading --color=always --smart-case --hidden -g "!.git" -- %s || true'
      let initial_command = printf(command_fmt, shellescape(a:query))
      let reload_command = printf(command_fmt, '{q}')
      let spec = {'options': ['--phony', '--query', a:query, '--bind', 'change:reload:'.reload_command, '--exact', '--pointer=@', '-i', '-m', '--cycle', '--border=none', '--marker=*', '--ansi', '--preview-window=left,50%', '--bind=alt-bspace:backward-kill-word,ctrl-x:beginning-of-line+kill-line,ctrl-a:select-all', '--color=16,fg+:bright-red,hl:bright-blue,hl+:green,query:blue,prompt:yellow,info:magenta,pointer:bright-yellow,marker:bright-blue,spinner:bright-blue,header:blue', '--prompt=content from files(+hidden) (git ignored) at . > ']}
      let spec = fzf#vim#with_preview(spec, 'right', 'ctrl-/')
      call fzf#vim#grep(initial_command, 1, fzf#vim#with_preview(spec), a:fullscreen)
    endfunction

    command! -nargs=* -bang SCIF call SearchContentInFiles(<q-args>, <bang>0)
    nnoremap <silent> <S-F> :SCIF!<CR>

" Delete trailing white space on save, useful for some filetypes ;)
    fun! CleanExtraSpaces()
        let save_cursor = getpos(".")
        let old_query = getreg('/')
        silent! %s/\s\+$//e
        call setpos('.', save_cursor)
        call setreg('/', old_query)
    endfun

    if has("autocmd")
        autocmd BufWritePre *.txt,*.js,*.py,*.wiki,*.sh,*.coffee :call CleanExtraSpaces()
    endif

call plug#begin('~/.vim/plugged')

"Plug 'junegunn/vim-easy-align'
"Plug 'https://github.com/junegunn/vim-github-dashboard.git'
"Plug 'SirVer/ultisnips' | Plug 'honza/vim-snippets'
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
Plug 'tiagofumo/vim-nerdtree-syntax-highlight'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
"Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'ryanoasis/vim-devicons'
Plug 'mboughaba/i3config.vim',       { 'for' : 'i3config' }
Plug 'dag/vim-fish',                 { 'for' : 'fish' }
Plug 'maralla/completor.vim', { 'for' : ['fish', 'vim'] }
Plug 'junegunn/vim-peekaboo'
Plug 'itchyny/lightline.vim'
"Plug 'sickill/vim-monokai'
Plug 'chrisbra/colorizer'
"Plug 'sheerun/vim-polyglot'
Plug 'sainnhe/everforest'
Plug 'sainnhe/edge'
Plug 'sainnhe/sonokai'
Plug 'francoiscabrol/ranger.vim' "leader +f
Plug 'mhinz/vim-startify'
Plug 'catppuccin/vim', { 'as': 'catppuccin' }
"Plug 'dense-analysis/ale'
Plug 'Yggdroot/indentLine'
Plug 'vimwiki/vimwiki'
Plug 'RRethy/vim-illuminate'
Plug 'machakann/vim-highlightedyank'
Plug 'mcchrish/nnn.vim'

autocmd FileType text,markdown let b:vcm_tab_complete = 'dict'

call pathogen#infect()

let g:lightline = {
            \ 'colorscheme': 'everforest',
            \ 'active': {
            \   'left': [ [ 'mode', 'paste' ], [ 'readonly', 'absolutepath', 'modified' ] ],
            \ },
            \ }
if &term == 'xterm-kitty'
    let &t_ut=''
endif
if &term == 'rxvt-unicode-256color'
    let &t_ut=''
endif
if &term == 'alacritty'
    let &t_ut=''
endif

call plug#end()

let g:startify_fortune_use_unicode = 1
let g:startify_custom_header =
            \ startify#pad(split(system('fortune | cowsay -f tux'), '\n'))

" ColorSchemes
"
" important!!
        if has('termguicolors')
          set termguicolors
        endif
" For dark version.
        set background=dark
" For light version.
"       set background=light
" Available values: 'hard', 'medium'(default), 'soft'
        let g:everforest_background = 'hard'
        let g:everforest_better_performance = 1

"The configuration options should be placed before `colorscheme edge`.
"Available values:   `'default'`, `'aura'`, `'neon'`
        let g:edge_style = 'aura'
        let g:edge_better_performance = 1

" The configuration options should be placed before `colorscheme sonokai`.
"Available values:   `'default'`, `'atlantis'`, `'andromeda'`, `'shusia'`, `'maia'`, `'espresso'`
        let g:sonokai_style = 'espresso'
        let g:sonokai_better_performance = 1

"colorscheme sonokai
colorscheme everforest
"colorscheme edge
"colorscheme nord
"colorscheme onedark
"colorscheme one
"colorscheme monokai-pro

"set cursorcolumn 
set cursorline
hi Conceal      term=reverse    cterm=none      ctermfg=237     ctermbg=none
hi Cursorline   term=underline  cterm=bold      ctermfg=none    ctermbg=238
hi NonText      term=reverse    cterm=none      ctermfg=237     ctermbg=none
hi Normal       term=none       cterm=none      ctermfg=252     ctermbg=234
hi SpecialKey   term=reverse    cterm=none      ctermfg=14      ctermbg=23
hi Cursorcolumn   term=reverse  cterm=bold      ctermfg=12    ctermbg=227
    
    if &term == "tmux-256color"
        set t_Co=256
    endif
    
    if !has('gui_running') && &term =~ '^\%(screen\|tmux\)'
        " Better mouse support, see  :help 'ttymouse'
        set ttymouse=sgr

        " Enable true colors, see  :help xterm-true-color
        let &termguicolors = v:true
        let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
        let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"

        " Enable bracketed paste mode, see  :help xterm-bracketed-paste
        let &t_BE = "\<Esc>[?2004h"
        let &t_BD = "\<Esc>[?2004l"
        let &t_PS = "\<Esc>[200~"
        let &t_PE = "\<Esc>[201~"

        " Enable focus event tracking, see  :help xterm-focus-event
        let &t_fe = "\<Esc>[?1004h"
        let &t_fd = "\<Esc>[?1004l"
        execute "set <FocusGained>=\<Esc>[I"
        execute "set <FocusLost>=\<Esc>[O"

        " Enable modified arrow keys, see  :help arrow_modifiers
        execute "silent! set <xUp>=\<Esc>[@;*A"
        execute "silent! set <xDown>=\<Esc>[@;*B"
        execute "silent! set <xRight>=\<Esc>[@;*C"
        execute "silent! set <xLeft>=\<Esc>[@;*D"
    endif

    if exists('+termguicolors')
        let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
        let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
        set termguicolors
    endif

" Toggle signcolumn. Works on vim>=8.1 or NeoVim
function! ToggleSignColumn()
    if !exists("b:signcolumn_on") || b:signcolumn_on
        set signcolumn=no
        let b:signcolumn_on=0
    else
        set signcolumn=number
        let b:signcolumn_on=1
    endif
endfunction

