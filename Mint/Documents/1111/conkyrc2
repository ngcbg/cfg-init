conky.config = {
--
--   Conky Settings
--
background=true,
update_interval=1,
double_buffer=true,
no_buffers=true,

draw_shades=true,
draw_outline=false,
draw_borders=false,
draw_graph_borders=false,


--
--   Window Specifications
--
gap_x=-300,
gap_y=0,
alignment="tm",
minimum_size=600, 380,
maximum_width=600,

own_window=yes
own_window_type="normal",
--own_window_transparent=true,
own_window_hints="undecorate,sticky,skip_taskbar,skip_pager,below",

own_window_argb_visual=yes
own_window_argb_value=255


--
--   Text Settings
--
use_xft=yes
xftalpha=0
xftfont=sans serif:size=10
override_utf8_locale=yes
imlib_cache_size=0
draw_outline=no -- yes/no - amplify text


--
--   Color Scheme
--
default_color FFFFFF

color0='FFFFFF'   -- clock
color1='FFFFFF'   -- date
color2='e65c00'   -- current temperature
color3='ffa64d'   -- high tempratures
color4='668cff'   -- low tempratures
color5='FFFFFF'   -- days


--
--   Icon Sources
--
template0 '~/.conky-vision-icons/#fff__32',  -- today
template1 '~/.conky-vision-icons/#fff__32',  -- +1day
template2 '~/.conky-vision-icons/#fff__32',  -- +2days
template3 '~/.conky-vision-icons/#fff__32',  -- +3days
template4 '~/.conky-vision-icons/#fff__32',  -- +4days


--#====================================
--#   API Key
--#====================================
template6="e6837ca375f512da0cf85699204a7f53",


--
--   City ID
--
template7="727011",


--
--   Temp Unit (default, metric, imperial)
--
template8='metric',


--
--   Locale (e.g. "es_ES.UTF-8")
--   Leave empty for default
--   Leave the quotes
--
template9 ""

}

TEXT
------------------------------------------
--   CURL
------------------------------------------
\
\
${execi 300 l=${template9}; l=${l%%_*}; curl -s "api.openweathermap.org/data/2.5/forecast/daily?APPID=${template6}&id=${template7}&cnt=5&units=${template8}&lang=$l" -o ~/.cache/forecast.json}\
${execi 300 l=${template9}; l=${l%%_*}; curl -s "api.openweathermap.org/data/2.5/weather?APPID=${template6}&id=${template7}&cnt=5&units=${template8}&lang=$l" -o ~/.cache/weather.json}\
\
\
------------------------------------------
--   Clock
------------------------------------------
\
\
${font Poiret One:weight=Light:size=96}${color0}\
${alignc}${time %H:%M}\
${font}${color}
\
\
------------------------------------------
--   Date
------------------------------------------
\
\
${font Poiret One:weight=Light:size=28}${color1}\
${voffset 30}\
${alignc}${execi 300 LANG=${template9} LC_TIME=${template9} date +"%A, %B %d"}\
${font}${color}
\
\
------------------------------------------
--   Current Temperature
------------------------------------------
\
\
${font Poiret One:weight=Bold:size=22}${color2}\
${voffset 36}\
${goto 60}${execi 300 jq .main.temp ~/.cache/weather.json | awk '{print int($1+0.5)}' --# round num}°\
${font}${color}\
\
\
------------------------------------------
--   High Temperatures
------------------------------------------
\
\
${font Poiret One:weight=Bold:size=18}${color3}\
${goto 164}${execi 300 jq .list[1].temp.max ~/.cache/forecast.json | awk '{print int($1+0.5)}' --# round num}°\
${goto 272}${execi 300 jq .list[2].temp.max ~/.cache/forecast.json | awk '{print int($1+0.5)}' --# round num}°\
${goto 378}${execi 300 jq .list[3].temp.max ~/.cache/forecast.json | awk '{print int($1+0.5)}' --# round num}°\
${goto 484}${execi 300 jq .list[4].temp.max ~/.cache/forecast.json | awk '{print int($1+0.5)}' --# round num}°\
${font}${color}\
\
\
------------------------------------------
--   Low Temparatures
------------------------------------------
\
\
${font Poiret One:weight=Bold:size=14}${color4}\
${voffset 54}\
${goto 218}${execi 300 jq .list[1].temp.min ~/.cache/forecast.json | awk '{print int($1+0.5)}' --# round num}°\
${goto 324}${execi 300 jq .list[2].temp.min ~/.cache/forecast.json | awk '{print int($1+0.5)}' --# round num}°\
${goto 430}${execi 300 jq .list[3].temp.min ~/.cache/forecast.json | awk '{print int($1+0.5)}' --# round num}°\
${goto 536}${execi 300 jq .list[4].temp.min ~/.cache/forecast.json | awk '{print int($1+0.5)}' --# round num}°\
${font}${color}
\
\
------------------------------------------
--   Day Names
------------------------------------------
\
\
${font Poiret One:weight=Bold:size=16}${color5}\
${voffset 20}\
${goto 76}${execi 300 LANG=${template9} LC_TIME=${template9} date +%^a}\
${goto 182}${execi 300 LANG=${template9} LC_TIME=${template9} date -d +1day +%^a}\
${goto 288}${execi 300 LANG=${template9} LC_TIME=${template9} date -d +2days +%^a}\
${goto 394}${execi 300 LANG=${template9} LC_TIME=${template9} date -d +3days +%^a}\
${goto 500}${execi 300 LANG=${template9} LC_TIME=${template9} date -d +4days +%^a}\
${font}${color}
\
\
------------------------------------------
--   Weather Icons
------------------------------------------
\
\
${execi 300 cp -f ${template0}/$(jq .weather[0].id ~/.cache/weather.json).png ~/.cache/current.png}${image ~/.cache/current.png -p 72,278 -s 32x32}\
${execi 300 cp -f ${template1}/$(jq .list[1].weather[0].id ~/.cache/forecast.json).png ~/.cache/forecast-1.png}${image ~/.cache/forecast-1.png -p 178,278 -s 32x32}\
${execi 300 cp -f ${template2}/$(jq .list[2].weather[0].id ~/.cache/forecast.json).png ~/.cache/forecast-2.png}${image ~/.cache/forecast-2.png -p 284,278 -s 32x32}\
${execi 300 cp -f ${template3}/$(jq .list[3].weather[0].id ~/.cache/forecast.json).png ~/.cache/forecast-3.png}${image ~/.cache/forecast-3.png -p 390,278 -s 32x32}\
${execi 300 cp -f ${template4}/$(jq .list[4].weather[0].id ~/.cache/forecast.json).png ~/.cache/forecast-4.png}${image ~/.cache/forecast-4.png -p 496,278 -s 32x32}\
