! Save this file as ~/.Xdefaults and use "xrdb -merge ~/.Xdefaults" to activate the changes.
XTerm*color0: #000000
XTerm*color1: #cc0000
XTerm*color2: #3eaa06
XTerm*color3: #c4c000
XTerm*color4: #3465ff
XTerm*color5: #a5709b
XTerm*color6: #06b8ba
XTerm*color7: #eeeeee
XTerm*color8: #888888
XTerm*color9: #ef2929
XTerm*color10: #8ae234
XTerm*color11: #fce94f
XTerm*color12: #739fff
XTerm*color13: #ad7fa8
XTerm*color14: #34e2e2
XTerm*color15: #ffffff
