! Save this file as ~/.Xdefaults and use "xrdb -merge ~/.Xdefaults" to activate the changes.
XTerm*color0: #000000
XTerm*color1: #bb0000
XTerm*color2: #00bb00
XTerm*color3: #cb6e00
XTerm*color4: #0000cc
XTerm*color5: #b100bc
XTerm*color6: #00bbaa
XTerm*color7: #bbbbbb
XTerm*color8: #555555
XTerm*color9: #ff9933
XTerm*color10: #bbff00
XTerm*color11: #ffee00
XTerm*color12: #00aaff
XTerm*color13: #ff7fbb
XTerm*color14: #7fffff
XTerm*color15: #ffffff
