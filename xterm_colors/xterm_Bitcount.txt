! Save this file as ~/.Xdefaults and use "xrdb -merge ~/.Xdefaults" to activate the changes.
XTerm*color0: #333333
XTerm*color1: #b00000
XTerm*color2: #008100
XTerm*color3: #a17500
XTerm*color4: #3030ff
XTerm*color5: #e500e5
XTerm*color6: #00b3b3
XTerm*color7: #cccccc
XTerm*color8: #656565
XTerm*color9: #ff3838
XTerm*color10: #00c200
XTerm*color11: #d7d700
XTerm*color12: #8585ff
XTerm*color13: #ff9cff
XTerm*color14: #00efef
XTerm*color15: #ffffff
