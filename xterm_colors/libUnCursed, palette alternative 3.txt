! Save this file as ~/.Xdefaults and use "xrdb -merge ~/.Xdefaults" to activate the changes.
XTerm*color0: #000000
XTerm*color1: #dc322f
XTerm*color2: #859900
XTerm*color3: #b58900
XTerm*color4: #268bd2
XTerm*color5: #f33682
XTerm*color6: #2aa198
XTerm*color7: #eee8d5
XTerm*color8: #888888
XTerm*color9: #ef2929
XTerm*color10: #8ae234
XTerm*color11: #fce94f
XTerm*color12: #739fff
XTerm*color13: #ad7fa8
XTerm*color14: #34e2e2
XTerm*color15: #ffffff
