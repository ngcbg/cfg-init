! Save this file as ~/.Xdefaults and use "xrdb -merge ~/.Xdefaults" to activate the changes.
XTerm*color0: #000000
XTerm*color1: #cd0000
XTerm*color2: #00cd00
XTerm*color3: #cdcd00
XTerm*color4: #0000cd
XTerm*color5: #cd00cd
XTerm*color6: #00cdcd
XTerm*color7: #faebd7
XTerm*color8: #404040
XTerm*color9: #ff0000
XTerm*color10: #00ff00
XTerm*color11: #ffff00
XTerm*color12: #0000ff
XTerm*color13: #ff00ff
XTerm*color14: #00ffff
XTerm*color15: #ffffff
