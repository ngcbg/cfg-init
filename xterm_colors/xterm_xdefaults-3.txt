! Save this file as ~/.Xdefaults and use "xrdb -merge ~/.Xdefaults" to activate the changes.
XTerm*color0: #000000
XTerm*color1: #aa0000
XTerm*color2: #00aa00
XTerm*color3: #aa5500
XTerm*color4: #0000aa
XTerm*color5: #aa00aa
XTerm*color6: #00aaaa
XTerm*color7: #aaaaaa
XTerm*color8: #555555
XTerm*color9: #ff5555
XTerm*color10: #55ff55
XTerm*color11: #ffff55
XTerm*color12: #5555ff
XTerm*color13: #ff55ff
XTerm*color14: #55ffff
XTerm*color15: #ffffff
