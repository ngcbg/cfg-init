#!/usr/bin/env python

import os
import random
import subprocess


class WallpaperManager:
    def __init__(self, state_file, output_file, max_file_length):
        """
        Initialize the WallpaperManager.

        Parameters:
        - state_file (str): Path to the file storing state information.
        - output_file (str): Path to the file where modified lines
        - will be written.
        - max_file_length (int): Maximum length of the file.
        """
        self.state_file = state_file
        self.output_file = output_file
        self.max_file_length = max_file_length

    def read_state(self):
        """
        Read the state from the state file.

        Returns:
        - list: List of strings representing the state.
        """
        try:
            with open(self.state_file, 'r') as file:
                return file.read().splitlines()
        except FileNotFoundError:
            return []

    def write_state(self, chosen_elements):
        """
        Write the state to the state file.

        Parameters:
        - chosen_elements (list): List of chosen elements.
        """
        with open(self.state_file, 'w') as file:
            for element in chosen_elements:
                file.write(element + '\n')

    def write_to_file(self, chosen_elements):
        """
        Write the modified lines to the output file.

        Parameters:
        - chosen_elements (list): List of chosen elements.
        """
        with open(self.output_file, 'w') as file:
            for modified_line in chosen_elements:
                file.write(modified_line + '\n')

    def prefix_lines(self):
        """
        Add prefix strings to the last two lines and write to the output file.
        """
        with open(self.state_file, 'r') as file:
            lines = file.readlines()
            last_two_lines = lines[-2:]

        prefix_strings = [
           'nitrogen --head=0 --set-zoom-fill ',
           'nitrogen --head=1 --set-zoom-fill '
        ]
        modified_lines = [
            prefix + line.strip()
            for prefix, line in zip(prefix_strings, last_two_lines)
        ]

        self.write_to_file(modified_lines)

        for line in modified_lines:
            print(line)

    def crawl_directory(self, directory_path):
        """
        Crawl a directory and return a list of files.

        Parameters:
        - directory_path (str): Path to the directory.

        Returns:
        - list: List of file paths.
        """
        file_list = []
        for root, dirs, files in os.walk(directory_path):
            for file in files:
                file_list.append(os.path.join(root, file))
        return file_list

    def choose_random_elements(self, file_list, chosen_elements, state):
        """
        Choose random elements from the file list and update state.

        Parameters:
        - file_list (list): List of file paths.
        - chosen_elements (list): List to store chosen elements.
        - state (list): List representing the state.

        Returns:
        - list: List of random chosen elements.
        """
        remaining_files = set(file_list) - set(state)
        if len(remaining_files) < 2:
            print("Not enough remaining files. Resetting state.")
            state.clear()
            remaining_files = file_list

        remaining_files_list = list(remaining_files)

        random_elements = random.sample(remaining_files_list, 2)
        chosen_elements.extend(random_elements)
        state.extend(random_elements)
        return random_elements

    def main(self):
        """
        Main entry for the script.
        """
        directory1 = "/home/ngc/Pictures/Wallpaper/new"
        directory2 = "/home/ngc/Pictures/Wallpaper/nord-background-main"

        state = self.read_state()
        file_list = self.crawl_directory(
            directory1) + self.crawl_directory(directory2)

        chosen_elements = []
        random_elements = self.choose_random_elements(
            file_list, chosen_elements, state
        )

        self.write_to_file(random_elements)
        self.write_state(state)
        self.prefix_lines()


if __name__ == "__main__":
    # Create an instance of WallpaperManager and run the main method
    manager = WallpaperManager(
        state_file="/home/ngc/script_state.txt",
        output_file="/home/ngc/wallpaper_files.txt",
        max_file_length=20,
    )
    manager.main()

    subprocess.run(['sh', '/home/ngc/wallpaper_files.txt'])
