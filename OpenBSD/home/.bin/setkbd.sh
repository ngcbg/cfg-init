#!/bin/sh

layout=`setxkbmap -query | sed -rn 's/layout.*(..)/\1/p'`
case $layout in
    us)
    setxkbmap bg
    pgrep -x dunst >/dev/null && notify-send -i kxkb -u low "[Layout] Bulgarian"
    ;;

bg)
setxkbmap us
pgrep -x dunst >/dev/null && notify-send -i kxkb -u low "[Layout] English"
;;
esac
