#!/bin/bash

#
# Dependencies: fzf 

# This is the path to your movies folder
movies="/run/media/ngc/NAS/Movies/"

menu=$(ls -a "$movies" | uniq -u | fzf --height=100% --reverse --header-first)

#exec nohup mpv "$movies"/"$menu">/dev/null 2>&1 
exec nohup mpv "$movies"/"$menu">/dev/null 2>&1 

