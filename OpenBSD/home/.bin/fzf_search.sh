#!/usr/bin/env bash
#
export FZF_DEFAULT_COMMAND="fd --type f . \
  $HOME/.bin \
  $HOME/Documents \
  $HOME/Music \
  $HOME/Downloads \
  $HOME/.config \
  "
  #selected="$(fzf -i --delimiter / --with-ntp -2,-1)"
  selected="$(fzf -i --delimiter /  --preview 'bat --color=always {}'  --multi  --cycle --keep-right -1 \
--layout=reverse  --info=default \
--border=double \
--info=inline \
--prompt='$>' \
--pointer='→' \
--marker='♡' \
--header='CTRL-c or ESC to quit' \
--color='dark,fg:magenta'
 )"

  trap "" HUP

  if [[ $1 = "dir" ]]; then
    echo "${selected}" | xclip -selection clipboard >/dev/null 2>&1 &
    thunar "$selected" >/dev/null 2>&1 &
  else
    xdg-open "$selected" >/dev/null 2>&1 &
  fi
