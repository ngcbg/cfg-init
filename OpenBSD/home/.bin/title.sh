#!/bin/sh 
set -u

main() {
  window_id=`xprop -root 32x '\t$0' _NET_ACTIVE_WINDOW | awk {'print $2'}`
  title=`xwininfo -id "window_id" | perl -ne 'print $1 if /"(.*)"\s*$/'`

  if ! tempfile=`mktemp`  
  then
    echo "Cannot create temporary file" 1>&2
    exit 1
  fi

  echo "$title" > "$tempfile"

  popup_window_title="Choose new title in editor, then save and exit..."

  xterm -T "$popup_window_title" -n "$popup_window_title" \
    -e "${EDITOR:-vi}" "$tempfile"

  newtitle=`head -c 1000 "$tempfile"`

  xwit -id "$window_id" -name "$newtitle"
}

main "$@"
