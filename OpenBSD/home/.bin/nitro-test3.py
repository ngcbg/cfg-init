#!/usr/bin/env python

import os
import random

state_file = "/home/ngc/script_state.txt"
output_file = "/home/ngc/wallpaper_files.txt"


def read_state():
    """ Read from the state file and then returns an array"""
    try:
        with open(state_file, 'r') as f:
            return f.read().splitlines()
    except FileNotFoundError:
        return []


def write_state(chosen_elements):
    with open(state_file, 'w') as f:
        for element in chosen_elements:
            f.write(element + '\n')


def write_to_file(chosen_elements):

    with open(output_file, 'w') as f:
        for element in chosen_elements:
            f.write(f"{element}\n")


def trim_lines():

    with open(state_file, 'r') as file:
        last_two_lines = file.readlines()[-2:]

    prefix_strings = [
     'nitrogen --head=0 --set-zoom-fill ', 'nitrogen --head=1 --set-zoom-fill '
    ]

    modified_lines = [
     prefix + line.strip() for prefix, line in
     zip(prefix_strings, last_two_lines)
    ]

    with open(output_file, 'w') as file:
        for modified_line in modified_lines:
            file.write(modified_line + '\n')

    for line in modified_lines:
        print(line)


def crawl_directory(directory_path):
    file_list = []
    for root, dirs, files in os.walk(directory_path):
        for file in files:
            file_list.append(os.path.join(root, file))
    return file_list


def choose_random_elements(file_list, chosen_elements, state):
    remaining_files = set(file_list) - set(state)
    if len(remaining_files) < 2:
        print("Not enough remaining files. Resetting state.")
        state.clear()
        remaining_files = file_list

    remaining_files_list = list(remaining_files)

    random_elements = random.sample(remaining_files_list, 2)
    chosen_elements.extend(random_elements)
    state.extend(random_elements)
    return random_elements


def main():
    directory1 = "/home/ngc/Pictures/Wallpaper/new"
    directory2 = "/home/ngc/Pictures/Wallpaper/nord-background-main"

    state = read_state()
    file_list = crawl_directory(directory1) + crawl_directory(directory2)
    chosen_elements = []
    random_elements = choose_random_elements(file_list, chosen_elements, state)
    #    index_mapping = substitute_filenames_with_indexes(random_elements)
    #    print("Index Mapping:", index_mapping)

    write_to_file(random_elements)
    write_state(state)
    trim_lines()


if __name__ == "__main__":
    main()
