#!/bin/bash

if	pgrep -x xcompmgr &>"0"
then
	killall xcompmgr & echo "Xcompmgr OFF" >/tmp/xcompmgr.log 2>&1 &
	notify-send -t 2000 "Turning OFF Xcompmgr  " 
	/usr/bin/picom -b --vsync-use-glfinish --experimental-backends \
        --config /home/ngc/picom.NGC & echo "Picom ON" >/tmp/picom.log 2>&1 &
	notify-send -t 2000 "Turning ON Picom  "
	
elif pgrep -x picom &>"0" 
then 
   killall picom && echo "Picom OFF" >/tmp/picom.log 2>&1 &
   notify-send -t 2000 "Turning OFF Picom  " 			
else	
   notify-send -t 2000 "Turning ON Xcompmgr  "
   /usr/bin/xcompmgr -c -C -f -F -r4.2 -o.40 -D3 & echo "Xcompmgr ON" >/tmp/xcompmgr.log 2>&1 &
fi

exit 0
