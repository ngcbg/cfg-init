#!/usr/local/bin/bash

RofiMenu() {
    option1="🔵-  Start Picom"
    option2="❌ Stop Picom"
    option3="🔵  Start Xcompmgr"
    option4="❌ Stop Xcompmgr"
    option5="⚡ Restart current composite manager"
    option6="❓ What composite manager is running?"
    option7="⭕ Stop composite manager"

    options="$option1\n$option2\n$option3\n$option4\n$option5\n$option6\n$option7"

    choice=$(printf '%b' "$options" | rofi -config ~/.config/rofi/nord.rasi -dmenu )

    case "$choice" in
        "$option1")
            if pgrep -x picom &>"0";
            then notify-send -t 3000 "Picom is ON  "
                exit 0
            elif pgrep -x xcompmgr &>"0";
            then pkill xcompmgr & /usr/local/bin/picom -b --vsync-use-glfinish --config ~/picom.NGC 2>&1 & notify-send -t 3000 "Turning ON Picom  "
            else
                sleep 1 & /usr/local/bin/picom -b --vsync-use-glfinish --config ~/picom.NGC 2>&1 & notify-send -t 3000 "Turning ON Picom  "
            fi
            ;;
        "$option2")
            if pgrep -x picom &>"0";
                then pkill picom & notify-send -t 3000 "Turning OFF Picom  "
            else
                notify-send -t 3000 "Picom is OFF  "
                exit 0
            fi
            ;;
        "$option3")
            if pgrep -x xcompmgr &>"0";
                then notify-send -t 3000 "Xcompmgr is ON  "
                exit 0
            elif pgrep -x picom &>"0";
                then pkill picom & sleep 1
                /usr/X11R6/bin/xcompmgr -c -C -f -F -r4.2 -o.40 -D3 2>&1 &
                notify-send -t 3000 "Turning ON Xcompmgr  "
            else
                sleep 1 /usr/X11R6/bin/xcompmgr -c -C -f -F -r4.2 -o.40 -D3 2>&1 &
                notify-send -t 3000 "Turning ON Xcompmgr  "
            fi
            ;;
        "$option4")
            if pgrep -x xcompmgr &>"0";
                then pkill xcompmgr & notify-send -t 3000 "Turning OFF Xcompmgr  "
            else
                notify-send -t 3000 "Xcompmgr is OFF  "
                exit 0
            fi
            ;;
        "$option5")
            if pgrep -x xcompmgr &>"0";
            then pkill xcompmgr & sleep 1
                /usr/X11R6/bin/xcompmgr -c -C -f -F -r4.2 -o.40 -D3 2>&1 & notify-send -t 3000 "Xcompmgr has been restarted"
            elif pgrep -x picom &>"0";
            then pkill picom & sleep 1
                /usr/local/bin/picom -b --vsync-use-glfinish --config ~/picom.NGC 2>&1 & notify-send -t 3000 "Picom has been restarted  "
            else
                notify-send -t 3000 "No active compositie manager"
            fi
            exit 0
            ;;
        "$option6")
            if pgrep -x xcompmgr &>"0";
                then notify-send -t 3000 " Xcompmgr is ON "
                exit 0
            elif pgrep -x picom &>"0";
                then notify-send -t 3000 " Picom is ON "
                exit 0
            else
                notify-send -t 3000 "No active composite manager"
                exit 0
            fi
            ;;
        "$option7")
            if pgrep -x xcompmgr &>"0";
                then pkill xcompmgr &
                notify-send -t 3000 "No active composite manager"
                exit 0
            elif pgrep -x picom &>"0";
                then pkill picom &
                notify-send -t 3000 "No active composite manager"
                exit 0
            else notify-send -t 3000 "No active composite manager"
                exit 0
            fi
            ;;
    esac
}

case "$1" in
    "--rofi-menu")
        RofiMenu;;
esac

