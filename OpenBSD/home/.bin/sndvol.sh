#!/bin/sh
#VAR=$(mixerctl -n outputs.master | cut -d , -f 1 | awk '{print $1/254*100}' | cut -d . -f 1)
VAR=$(sndioctl -n output.level | awk '{print $1*100}' | cut -d . -f 1)
VOL=${VAR}%
notify-send -u low "volume level:" "$VOL"
