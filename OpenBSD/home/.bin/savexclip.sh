#!/bin/sh

xclip -o | xclip -sel clip
echo $(xclip -o -sel clip) >> ~/.xclip_history
#sed -n '/^\s*$/d; G; s/\n/&&/; /^\([ -~]*\n\).*\n\1/d; s/\n//; h; P' -i ~/.xclip_history > /dev/null 2>&1
notify-send -u low "CLIPBOARD:" "'$(xclip -o -sel clip)' saved." &
