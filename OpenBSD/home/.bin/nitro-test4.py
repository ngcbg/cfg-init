#!/usr/bin/env python

import os
import random

state_file = os.path.join("/", "home", "ngc", "script_state.txt")
output_file = os.path.join("/", "home", "ngc", "wallpaper_files.txt")
max_file_length = 20


def read_state():
    """Read the state from the state file."""
    try:
        with open(state_file, 'r') as file:
            return file.read().splitlines()
    except FileNotFoundError:
        return []


def write_state(chosen_elements):
    """Write the state to the state file."""
    with open(state_file, 'w') as file:
        for element in chosen_elements:
            file.write(element + '\n')


def write_to_file(chosen_elements):
    """Write the modified lines to the output file."""
    with open(output_file, 'w') as file:
        for modified_line in chosen_elements:
            file.write(modified_line + '\n')


def trim_lines():
    """Trim the last two lines and write to the output file."""
    with open(state_file, 'r') as file:
        lines = file.readlines()
        last_two_lines = lines[-2:]

    prefix_strings = ['tar ', 'cat ']
    modified_lines = [prefix + line.strip() for prefix, line in zip(prefix_strings, last_two_lines)]

    write_to_file(modified_lines)

    for line in modified_lines:
        print(line)


def crawl_directory(directory_path):
    file_list = []
    for root, dirs, files in os.walk(directory_path):
        for file in files:
            file_list.append(os.path.join(root, file))
    return file_list


def choose_random_elements(file_list, chosen_elements, state):
    remaining_files = set(file_list) - set(state)
    if len(remaining_files) < 2:
        print("Not enough remaining files. Resetting state.")
        state.clear()
        remaining_files = file_list

    remaining_files_list = list(remaining_files)

    random_elements = random.sample(remaining_files_list, 2)
    chosen_elements.extend(random_elements)
    state.extend(random_elements)
    return random_elements


def main():
    directory1 = "/home/ngc/Pictures/Wallpaper/new"
    directory2 = "/home/ngc/Pictures/Wallpaper/nord-background-main"

    state = read_state()
    file_list = crawl_directory(directory1) + crawl_directory(directory2)
    chosen_elements = []
    random_elements = choose_random_elements(file_list, chosen_elements, state)
    #    index_mapping = substitute_filenames_with_indexes(random_elements)
    #    print("Index Mapping:", index_mapping)

    write_to_file(random_elements)
    write_state(state)
    trim_lines()


if __name__ == "__main__":
    main()
