#!/bin/bash

# Define directory
dir="/run/media/ngc/NAS/Movies/"

# Get list of files in directory
files=$(ls $dir)

# Use rofi to display list of files and get user's selection
selected=$(echo "$files" | rofi -dmenu -i -p "Choose a movie:")

# If user selected a file, play it with MPV
if [[ -n $selected ]]; then
    mpv "$dir/$selected"
fi

