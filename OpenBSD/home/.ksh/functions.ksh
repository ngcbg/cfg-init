#!/usr/bin/env ksh
#

THIS_SHELL=`ps o command -p $$ | grep -v "^COMMAND$" | tr -d '-' | cut -d' ' -f1`
case "${THIS_SHELL##/**/}" in
  bash|ksh|zsh) ;;
  *) >&2 echo "This script probably wont work with your shell, so bailing out now...bye!";
     exit 1;;
esac

[[ "$(uname -s)" == "FreeBSD" ]] && OS="BSD"
[[ "$(uname)" == "OpenBSD" ]] && OS="BSD"

OHMY_DO=${OHMY_DO:-doas}

function 0xMF-sudo {
  print 'default to doas instead of sudo' >&2
  [[ "$1" == "-i" ]] \
    && doas su - \
    || doas "$@"
}

function 0xMF-declare {
  case "$1" in
    "-f") typeset -f $2 ;;
    "-F") typeset +f ;;
    "*" | "" ) functions ;;
  esac
}

function eman {
  [[ -z "$1" ]] && { print >&2 "Usage: eman man-page-with-EXAMPLES-section"; return; }
  man -Tascii $1 | col -bx | sed -n '/^EXAMPLES/,/^[A-Z]/p' | sed -nr '/^(EXAMPLES| |$)/p'
}

function dpkg-list {
  dpkg-query --list|awk -F' ' '{printf("%s\t%-32s\t",$1,substr($2,0,40));$1=$2=$3=$4=""; print $0}'
}

function 0xMF-pkg_locate {
  [ -z "$1" ] && echo "Usage: pkg_locate name" && return
  ports "$1"
}

function ports {
  [ -z "$1" ] && echo "Usage: ports name" && return
  pushd /usr/ports > /dev/null
  echo */*|tr ' ' '\n'|grep $1
  popd  > /dev/null
}

function lf { 
	fzf | xargs -r -I % xdg-open % ;
}
##
## From Oh My KSH framework - https://github.com/qbit/ohmyksh
##

port() {
	cd /usr/ports/*/$1 2>/dev/null || \
		cd /usr/ports/*/*/$1 2>/dev/null || \
		return
}

port_grep() {
	local _usage
	_usage="port_grep [file] [pattern]"
	[ -z $1 ] || [ -z $2 ] && echo $_usage
	[ ! -z $1 ] && [ ! -z $2 ] && find /usr/ports -iname "${1}" -exec grep -iH ${2} {} \;
}

pclean() {
	find . -name \*.orig -exec rm {} \;
	find . -size 0 -exec rm {} \;
}

revert_diffs() {
	for i in $(find . -name \*.orig); do
		F=$(echo $i | sed 's/\.orig//')
		mv -v "$i" "$F"
	done
}

cdw() {
	cd $(make show=WRKSRC)
}

maintains() {
	(
		cd /usr/ports/*/$1 > /dev/null 2>&1 && \
			make show=MAINTAINER || \
			echo "No port '/usr/ports/*/$1'"
	)
}

seq() {
	start=$1
	end=$2
	if echo "$start" | egrep -q '^[0-9]+$'; then
		if [ "$start" -lt "$end" ]; then
			while [ $start -le "${end}" ]; do
				printf "%d\\n" "$start"
				start=$((start + 1))
			done
		else
			while [ $start -ge "${end}" ]; do
				printf "%d\\n" "$start"
				start=$((start - 1))
			done
		fi
	else
		start=$(printf "%d" \'$start)
		end=$(printf "%d" \'$end)
		if [ "$start" -lt "$end" ]; then
			while [ $start -le "${end}" ]; do
				printf "\x$(printf %x $start)\\n"
				start=$((start + 1))
			done
		else
			while [ $start -ge "${end}" ]; do
				printf "\x$(printf %x $start)\\n"
				start=$((start - 1))
			done
		fi
	fi
}

zh() {
	fc -ln | eval $(fzf)
}

zpkg() {
	local _pkg _usage

	_usage="zpkg add|rm"

	if [ ! -f /usr/local/share/sqlports ]; then
		echo "please install sqlports"
		return 1
	fi

	if [ ! -e /usr/local/bin/sqlite3 ]; then
		echo "please install sqlite3"
		return 1
	fi

	if [ -z $1 ]; then
	       	echo $_usage
		return 1
	else
		case $1 in
			add)
				_pkg=$(/usr/local/bin/sqlite3 -separator " " \
					/usr/local/share/sqlports \
					"select distinct fullpkgname from Ports;" |\
				       	fzf --preview "/usr/sbin/pkg_info {1}")
				[ ! -z $_pkg ] && ${OHMY_DO} /usr/sbin/pkg_add $_pkg
				;;
			rm)
				_pkg=$(ls -1 /var/db/pkg | fzf --preview "/usr/sbin/pkg_info {1}")
				[ ! -z $_pkg ] && ${OHMY_DO} /usr/sbin/pkg_delete $_pkg
				;;
			*)
				echo $_usage
		esac
	fi
}

k() {
	${K_DEBUG}
	if [ -z $1 ]; then
		echo $PWD >> ~/.k
	else
		K=~/.k
		case $1 in
		clean)	sort -u $K -o ${K};;
		rm)	sed -i -E "\#^${2:-${PWD}}\$#d" ${K};;
		ls)	cat ${K};;
		*)	cd "$(grep -e "$1" ${K} | head -n 1)";;
		esac
	fi
}

function reload_completions {
	if [ -e ~/.ksh/ksh.completion ]; then
		. ~/.ksh/ksh.completion
		echo "ksh completions reloaded"
	fi
}

function fzf-histo {
    RES=$(fzf --tac --no-sort -e < $HISTFILE)
    test -n "$RES" || exit 0
    eval "$RES"
}

bind -m ^R=fzf-histo^J

# vim:nospell:ft=sh:
