# Setup fzf
# ---------
if [[ ! "$PATH" == */home/ngc/.fzf/bin* ]]; then
  PATH="${PATH:+${PATH}:}/home/ngc/.fzf/bin"
fi

eval "$(fzf --bash)"
