#!/usr/local/bin/env zsh

[[ -f ~/.zshrc-personal ]] && source ~/.zshrc-personal
[[ -f ~/.zsh_exports ]] && source ~/.zsh_exports
[[ -f ~/.zsh_aliasrc ]] && source ~/.zsh_aliasrc
[[ -f /usr/local/share/fzf/zsh/key-bindings.zsh ]] && source /usr/local/share/fzf/zsh/key-bindings.zsh
[[ -f /usr/local/share/fzf/zsh/completion.zsh ]] && source /usr/local/share/fzf/zsh/completion.zsh

setopt GLOB_DOTS
setopt appendhistory
HISTSIZE=10000
SAVEHIST=10000
HISTFILE=~/.cache/zshhistory
autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots)               # Include hidden files.

#unsetopt SHARE_HISTORY
setopt SHARE_HISTORY

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

if [ -d "$HOME/.bin" ] ;
  then PATH="$HOME/.bin:$PATH"
fi

if [ -d "$HOME/.local/bin" ] ;
  then PATH="$HOME/.local/bin:$PATH"
fi

#time neofetch
ufetch
#time pfetch
echo
	da
echo
	cal

# vim:nospell:ft=sh:

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
source <(fzf --zsh)
