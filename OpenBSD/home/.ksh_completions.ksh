#: | ssh | ssh known hosts |
read_known_hosts() {
        local _file=$1 _line _host

        while read _line ; do
          _line=${_line%%#*} # delete comments
          _line=${_line%%@*} # ignore markers
          _line=${_line%% *} # keep only host field

          [[ -z $_line ]] && continue

          local IFS=,
          for _host in $_line; do
            _host=${_host#\[}
            _host=${_host%%\]*}
            for i in ${HOST_LIST[*]}; do
              [[ $_host == $i ]] && continue 2
            done
            set -s -A HOST_LIST ${HOST_LIST[*]} $_host
          done
        done <$_file
}

[[ -s /etc/ssh/ssh_known_hosts ]] && read_known_hosts /etc/ssh/ssh_known_hosts
[[ -s ~/.ssh/known_hosts ]] && read_known_hosts ~/.ssh/known_hosts

set -A complete_ssh -- ${HOST_LIST[*]}
set -A complete_scp -- ${HOST_LIST[*]}
set -A complete_mosh -- ${HOST_LIST[*]}

# Package Managers
# OpenBSD pkg_*
if [ -d /var/db/pkg ]; then
	PKG_LIST=$(ls -1 /var/db/pkg)
	set -A complete_pkg_delete -- $PKG_LIST
	set -A complete_pkg_info -- $PKG_LIST

	set -A complete_pkg_1 -- add check create delete info
	set -A complete_pkg_2 -- $PKG_LIST
fi

# Utilities

#: | man | man pages

if [ ! -f /tmp/man_list ]; then
	MANPATH=/usr/share/man man -k Nm~. | cut -d\( -f1 | tr -d , | \
		sort | \
		uniq > /tmp/man_list
fi
set -A complete_man_1 -- $(cat /tmp/man_list | more)

#Kill
set -A complete_kill_1 -- -9 -HUP -INFO -KILL -TERM

# Ifconfig
set -A complete_ifconfig_1 -- $(ifconfig | grep ^[a-z] | cut -d: -f1)

# Signify
set -A complete_signify_1 -- -C -G -S -V
set -A complete_signify_2 -- -q -p -x -c -m -t -z
set -A complete_signify_3 -- -p -x -c -m -t -z

# Reload completions
function reload_completions {
	if [ -e ~/.ksh_completions ]; then
		. ~/.ksh_completions
		echo "ksh completions reloaded"
	fi
}

set -A complete_cal_1 -- -j '/nDisplay Julian dates (days one-based, numbered from January 1)' \
						 -m 'Display weeks starting on Monday instead of Sunday' \
						 -w 'Display week numbers in the month display' \
						 -y 'Display a calendar for the current year'

#: | rc | OpenBSD rc scripts and rc commands |
set -A complete_rcctl_1 -- disable enable get ls order set restart start stop
set -A complete_rcctl_2 -- $(rcctl ls all)

#: | git | add, fetch... |
set -A complete_git_1 -- \
	$(git --list-cmds=main) \
	$(git config --get-regexp ^alias\. | awk -F '[\. ]' '{ print $2 }')

