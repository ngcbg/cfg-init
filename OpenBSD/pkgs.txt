adwaita-icon-theme-40.1.1 base icon theme for GNOME
at-spi2-atk-2.38.0  atk-bridge for at-spi2
at-spi2-core-2.40.3 service interface for assistive technologies
atk-2.36.0          accessibility toolkit used by gtk+
bzip2-1.0.8p0       block-sorting file compressor, unencumbered
cairo-1.16.0        vector graphics library
dbus-1.12.20p1v0    message bus system
dconf-0.40.0        configuration backend system
desktop-file-utils-0.26 utilities for dot.desktop entries
fribidi-1.0.10      library implementing the Unicode Bidirectional Algorithm
gdiff-3.8           GNU versions of the diff utilities
gdk-pixbuf-2.42.6   image data transformation library
gettext-runtime-0.21p1 GNU gettext runtime libraries and programs
glib2-2.68.4        general-purpose utility library
graphite2-1.3.14    rendering for complex writing systems
gtk+3-3.24.30       multi-platform graphical toolkit
gtk-update-icon-cache-3.24.30 gtk+ icon theme caching utility
harfbuzz-2.9.1      text shaping library
hicolor-icon-theme-0.17 fallback theme of the icon theme specification
intel-firmware-20210608v0 microcode update binaries for Intel CPUs
inteldrm-firmware-20201218 firmware binary images for inteldrm(4) driver
iwm-firmware-20210512 firmware binary images for iwm(4) driver
jpeg-2.1.1v0        SIMD-accelerated JPEG codec replacement of libjpeg
libffi-3.3p1        Foreign Function Interface
libiconv-1.16p0     character set conversion library
librsvg-2.50.7      SAX-based render library for SVG files
libsigsegv-2.12     library for handling page faults in user mode
libslang-2.3.2      stack-based interpreter for terminal applications
libsodium-1.0.18p1  library for network communications and cryptography
libssh2-1.9.0       library implementing the SSH2 protocol
libxml-2.9.12       XML parsing library
lz4-1.9.3p0         fast BSD-licensed data compression
lzo2-2.10p2         portable speedy lossless data compression library
mc-4.8.27           free Norton Commander clone with many useful features
nano-5.8            simple editor, inspired by Pico
oniguruma-6.9.7.1   regular expressions library
pango-1.48.10       library for layout and rendering of text
pcre-8.44           perl-compatible regular expression library
png-1.6.37          library for manipulating PNG images
python-3.8.12       interpreted object-oriented programming language
quirks-4.53         exceptions to pkg_add rules
shared-mime-info-2.1 shared mime database for desktops
sqlite3-3.35.5p0    embedded SQL implementation
tiff-4.3.0          tools and library routines for working with TIFF images
unzip-6.0p14        extract, list & test files in a ZIP archive
uvideo-firmware-1.2p3 firmware binary images for uvideo(4) driver
vim-8.2.3456-gtk3   vi clone, many additional features
vmm-firmware-1.14.0 firmware binary images for vmm(4) driver
xz-5.2.5            LZMA compression and decompression tools
zip-3.0p1           create/update ZIP files compatible with PKZip(tm)
zstd-1.5.0          zstandard fast real-time compression algorithm
