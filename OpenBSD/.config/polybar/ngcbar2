[global/wm]
margin-top = 0
;margin-bottom = 0

[settings]
throttle-output = 8
throttle-output-for = 10
compositing-background = source
compositing-foreground = over
compositing-overline = over
compositing-underline = over
compositing-border = over
dpi-x = 0
dpi-y = 0
pseudo-transparency = false

# Define fallback values used by all module formats
;format-foreground = #FF0000
;format-background = #00FF00
;format-underline =
;format-overline =
;format-spacing =
;format-padding =
;format-margin =
;format-offset = #39475B

[colors]
#background = #803c5377
background =  #952c413a
background-i3 = #323232
background-alt = #444
foreground = #e4e4e4
foreground-alt = #555
primary = #ffb52a
secondary = #e60053
alert = #bd2c40
workspace-occupied = #7d63798
workspace-active = #D087705a
#define nord0 #2E3440
#define nord1 #3B4252
#define nord2 #434C5E
#define nord3 #4C566A
#define nord4 #D8DEE9
#define nord5 #E5E9F0
#define nord6 #ECEFF4
#define nord7 #8FBCBB
#define nord8 #88C0D0
#define nord9 #81A1C1
#define nord10 #5E81AC
#define nord11 #BF616A
#define nord12 #D08770
#define nord13 #EBCB8B
#define nord14 #A3BE8C
#define nord15 #B48EAD

################################################################################
############                    MAINBAR-i3                         #############
################################################################################

[bar/mainbar-i3]
monitor-fallback = HDMI-0
monitor = ${env:MONITOR:eDP1}
wm-name = i3
width = 100%
height = 26
bottom = false
#offset-x = 1%
#offset-y = 1%
radius = 8.0
fixed-center = true
#separator = |

background = ${colors.background-i3}
foreground = ${colors.foreground}

line-size = 2
line-color = #f00

#override-redirect = true
#screenchange-reload = true 
#wm-restack = i3

padding-left = 1
padding-right = 1

module-margin-left = 1
module-margin-right = 1

enable-ipc = true

# https://github.com/jaagr/polybar/wiki/Fonts
font-0 = "Arimo:style=Regular:size=10;1"
font-1 = "Iosevka Nerd Font Mono:size=11;1"
font-2 = "Font Awesome 5 Free:style=Regular:size:12;1"
font-3 = "Font Awesome 5 Free:style=Solid:size:12;1"
#font-0 = "terminus:pixelsize=14;1"


modules-left = i3 load-average sensor_cpu sensor_gpu
modules-center = xwindow
modules-right =  network battery date xkeyboard 

tray-position = right
tray-detached = false
tray-offset-x = 0
tray-offset-y = 0
tray-padding = 5
tray-margin = 5
tray-maxsize = 18
tray-scale = 0.8
tray-background = ${colors.background-i3}

cursor-click = pointer
cursor-scroll = ns-resize

scroll-up = i3-msg workspace next_on_output
scroll-down = i3-msg workspace prev_on_output

################################################################################
############                    MAINBAR-CWM                         ############
################################################################################

[bar/mainbar-cwm]
monitor-fallback = HDMI-0
monitor = ${env:MONITOR:eDP1}
wm-name = cwm
width = 100%
height = 24
#bottom = true
bottom = false
#offset-x = 1%
#offset-y = 1%
#radius = 8.0
fixed-center = true
#separator = |

background = ${colors.background}
foreground = ${colors.foreground}

line-size = 2
line-color = #f00

override-redirect = true
screenchange-reload = true 
#wm-restack = i3

#border-size = 4
#border-left-size = 25
#border-right-size = 25
#border-top-size = 0
#border-bottom-size = 25
#border-color = #00000000

padding-left = 1
padding-right = 1

#module-margin-left = 2
module-margin-right = 1

# Enable support for inter-process messaging
enable-ipc = true

# https://github.com/jaagr/polybar/wiki/Fonts
#font-0 = "IcoMoon-Free:pixelsize=11;2"
;font-1 = "Feather:size=14;2"
font-0 = "Arimo:style=Regular:size=11;3"
font-1 = "Iosevka Nerd Font Mono:size=18;5"
;font-3 = "Font Awesome 5 Free:style=Regular:size:12;1"
;font-4 = "Font Awesome 5 Free:style=Solid:size:12;1"

modules-left = xworkspacescwm load-average sensor_cpu sensor_gpu
modules-center = xwindow
modules-right =  network date xkeyboard battery

tray-position = right
tray-detached = false
tray-offset-x = 0
tray-offset-y = 0
tray-padding = 2
tray-maxsize = 20
tray-scale = 1.0
tray-background = ${colors.background}

cursor-click = pointer
cursor-scroll = ns-resize

;scroll-up = fvwm3-desknext
;scroll-down = fvwm3-deskprev

################################################################################
############                    MAINBAR-FVWM                        ############
################################################################################

[bar/mainbar-fvwm]
monitor-fallback = HDMI-0
monitor = ${env:MONITOR:eDP1}
wm-name = fvwm
width = 100%
height = 24
#bottom = true
bottom = false
#offset-x = 1%
#offset-y = 1%
#radius = 8.0
fixed-center = true
#separator = |

background = ${colors.background}
foreground = ${colors.foreground}

line-size = 2
line-color = #f00

override-redirect = true
#screenchange-reload = true 
#wm-restack = i3

#border-size = 4
#border-left-size = 25
#border-right-size = 25
#border-top-size = 0
#border-bottom-size = 25
#border-color = #00000000

padding-left = 1
padding-right = 1

#module-margin-left = 2
module-margin-right = 1

# Enable support for inter-process messaging
enable-ipc = true

# https://github.com/jaagr/polybar/wiki/Fonts
font-0 = "IcoMoon-Free:pixelsize=11;2"
;font-1 = "Feather:size=14;2"
#;font-0 = "Arimo:style=Regular:size=10;1"
font-1 = "Iosevka Nerd Font Mono:size=24;6"
;font-3 = "Font Awesome 5 Free:style=Regular:size:12;1"
;font-4 = "Font Awesome 5 Free:style=Solid:size:12;1"

modules-left = load-average sensor_cpu sensor_gpu
modules-center = xwindow
modules-right =  network date xkeyboard

tray-position = right
tray-detached = false
tray-offset-x = 0
tray-offset-y = 0
tray-padding = 2
tray-maxsize = 20
tray-scale = 1.0
tray-background = ${colors.background}

cursor-click = pointer
cursor-scroll = ns-resize

scroll-up = fvwm-desknext
scroll-down = fvwm-deskprev

################################################################################
############                    MODULES NGC                         ############
################################################################################

[module/xwindow]
type = internal/xwindow
label =   %title:0:50:...%
format-padding = 2

#--------------------------------------------------------

[module/xworkspaces]
type = internal/xworkspaces
pin-workspaces = false
enable-click = true
enable-scroll = true
reverse-scroll = true

format = <label-state>
label-visible = %icon%
;format-padding = 1
icon-0 = ▰▱▱▱;🌑
icon-1 = ▱▰▱▱;🌓
icon-2 = ▱▱▰▱;🌕
icon-3 = ▱▱▱▰;🌗

label-active = %icon%
label-active-foreground = ${colors.foreground}
label-active-background = ${colors.workspace-active}
label-active-underline= #aaffc32a
label-active-padding = 2

label-occupied = %icon%
label-occupied-foreground = ${colors.foreground}
label-occupied-background = ${colors.workspace-occupied}
label-occupied-padding = 2

label-urgent = %icon%
label-urgent-foreground = ${colors.foreground}
label-urgent-background = ${colors.alert}
label-urgent-underline = ${colors.alert}
label-urgent-padding = 2

label-empty = %icon%
label-empty-foreground = ${colors.foreground}
format-foreground = ${colors.foreground}
format-background = ${colors.background}
label-empty-padding = 2

#--------------------------------------------------------

[module/xworkspacesfvwm]
type = internal/xworkspaces
pin-workspaces = false
enable-click = true
enable-scroll = true
reverse-scroll = true

format = <label-state>
label-visible = %icon%
;format-padding = 1
icon-0 = Main;▰▱▱
icon-1 = Desk1;▱▰▱
icon-2 = Desk2;▱▱▰

label-active = %icon%
label-active-foreground = ${colors.foreground}
label-active-background = ${colors.background}
label-active-underline= #95ffb52a
label-active-padding = 2

label-occupied = %icon%
label-occupied-background = ${colors.background}
label-occupied-padding = 2

label-urgent = %icon%
label-urgent-foreground = ${colors.foreground}
label-urgent-background = ${colors.alert}
label-urgent-underline = ${colors.alert}
label-urgent-padding = 2

label-empty = %icon%
label-empty-foreground = ${colors.foreground}
label-empty-padding = 2
format-foreground = ${colors.foreground}
format-background = ${colors.background}

#--------------------------------------------------------

[module/xworkspacescwm]
type = internal/xworkspaces
pin-workspaces = false
enable-click = true
enable-scroll = true
reverse-scroll = true

format = <label-state>
label-visible = %icon%
;format-padding = 1
icon-0 = nogroup;0
icon-1 = one;1
icon-2 = two;2
icon-3 = three;3
icon-4 = four;4
icon-5 = five;5
icon-6 = six;6
icon-7 = seven;7
icon-8 = eight;8
icon-9 = nine;9

label-active = %icon%
label-active-foreground = #7BA1B3
label-active-background = #000000
label-active-underline= #F08080
label-active-padding = 2

label-occupied = %icon%
label-occupied-foreground = #7BA1B3
label-occupied-background = ${colors.background}
label-occupied-underline= #F08080
label-occupied-padding = 2

label-urgent = %icon%
label-urgent-foreground = #c0c5ce
label-urgent-background = ${colors.alert}
label-urgent-underline = ${colors.alert}
label-urgent-padding = 2

label-empty = %icon%
label-empty-foreground = #c0c5ce
format-foreground = ${colors.foreground}
format-background = ${colors.background}
label-empty-padding = 2

#--------------------------------------------------------

[module/xkeyboard]
type = internal/xkeyboard
blacklist-0 = num lock
blacklist-1 = scroll lock
#format-prefix = "  "
#format-prefix-foreground = ${colors.foreground}
label-layout = %layout%
label-layout-padding = 1
label-layout-margin = 1
label-indicator-background = ${colors.secondary}
#label-indicator-underline = ${colors.secondary}
#format-underline = #95ffb52a

#--------------------------------------------------------

[module/filesystem]
type = internal/fs
interval = 120
mount-0 = /
mount-1 = /home
label-mounted =   %mountpoint% %percentage_used%%
label-unmounted = %mountpoint% not mounted
label-unmounted-foreground = ${colors.foreground-alt}

#--------------------------------------------------------

[module/cpu]
type = internal/cpu
interval = 10
;format-padding = 1
format-prefix = " "
format-prefix-foreground = #aaffc32a
label-indicator-background = ${colors.secondary}
label = %percentage:2%%

#--------------------------------------------------------

[module/memory]
type = internal/memory
interval = 10
format-padding = 1
format-prefix = " "
format-prefix-foreground = #aaffc32a
format-foreground = ${colors.foreground}
format-background = ${colors.background}
label = %percentage_used%% %mb_used%

#--------------------------------------------------------

[module/eth]
type = internal/network
interface = enp8s0
interval = 10
format-connected-prefix = " "
label-indicator-background = ${colors.secondary}
format-connected-prefix-foreground = ${colors.foreground}
label-connected = %local_ip%
format-disconnected-prefix = 

#--------------------------------------------------------

[module/date]
type = internal/date
interval = 5
date =
date-alt = " %m.%d.%Y"
time = %H:%M
time-alt = %A %H:%M
#format-prefix = " "
label-padding = 1
label-margin = 1
format-prefix-foreground = ${colors.foreground}
label = %date% %time%
#format-underline = #95ffb52a

#--------------------------------------------------------

[module/pulseaudio]
type = internal/pulseaudio
sink = alsa_output.pci-0000_12_00.3.analog-stereo
use-ui-max = true
interval = 5
format-volume = <ramp-volume> <label-volume>
label-muted =  Muted
label-muted-foreground = ${colors.foreground}
ramp-volume-0 = 
ramp-volume-1 = 
ramp-volume-2 = 
ramp-volume-3 = 
ramp-volume-4 = 
format-underline = #95ffb52a

#--------------------------------------------------------

[module/memreal]
type = custom/script
exec = ~/.bin/mem-real.sh
interval = 10
format-foreground = ${colors.foreground}
#format-background = ${colors.background}
format-prefix = "  "
format-prefix-foreground = #aaffc32a
label = %output:0:150:%

#--------------------------------------------------------

[module/cpu-temp]
type = custom/script
exec = ~/.config/polybar/scripts/tempcores2.sh
#exec = ~/.bin/cpu_temp.sh
interval = 10
format-foreground = ${colors.foreground}
#format-background = ${colors.background}
format-prefix = "  "
format-prefix-foreground = #aaffc32a
label = %output:0:150:%

#--------------------------------------------------------

[module/cpu-fan]
type = custom/script
exec = ~/.bin/cpu_fan.sh
interval = 10
format-foreground = ${colors.foreground}
#format-background = ${colors.background}
format-prefix = " "
format-prefix-foreground = #aaffc32a
label = %output%

#--------------------------------------------------------

[module/gpu-temp]
type = custom/script
exec = ~/.bin/nvidia_temp.sh
interval = 10
format-foreground = ${colors.foreground}
#format-background = ${colors.background}
format-prefix = " "
format-prefix-foreground = #aaffc32a
label = %output%

#--------------------------------------------------------

[module/menu]
type = custom/script
exec = echo ""
click-left = "jgmenu_run >/dev/null 2>&1 &"
format-spacing = 1
format-underline = #95ffb52a
format-padding = 1

#--------------------------------------------------------

[module/window_switch]
type = custom/script
#interval = 5
click-left = skippy-xd
click-right = skippy-xd
exec = echo "  "
label = %output%
format-underline = #95ffb52a

#--------------------------------------------------------

[module/music]
type = custom/script
interval = 1
label = %output%
exec = ~/.bin/mussic_polybar.sh

#--------------------------------------------------------

[module/load-average]
type = custom/script
exec = uptime | grep -ohe 'load average[s:][: ].*' | awk '{ print $3" "$4" "$5"," }' | sed 's/,//g'
#;exec = uptime | grep 'load average' | awk '{ print $8" "$9" "$10"," }' | sed 's/,//g'
interval = 10
label = %output:10%
label-padding = 2
format-foreground = ${colors.foreground}
#format-background = ${colors.background}
format-prefix = "  "
#format-prefix-foreground = #aaffc32a
;format-underline = #95ffb52a

#--------------------------------------------------------

[module/i3]
;https://github.com/jaagr/polybar/wiki/Module:-i3
type = internal/i3
pin-workspaces = false
strip-wsnumbers = false
index-sort = true
enable-click = true
enable-scroll = true
wrapping-scroll = false
reverse-scroll = false
fuzzy-match = false

;http://fontawesome.io/cheatsheet/
;       v     

ws-icon-0 = 1;➊
ws-icon-1 = 2;➋
ws-icon-2 = 3;➌
ws-icon-3 = 4;➍
ws-icon-4 = 5;➎
ws-icon-5 = 6;➏
ws-icon-6 = 7;➐
ws-icon-7 = 8;➑
ws-icon-8 = 9;➒
ws-icon-9 = 10;➓
ws-icon-default = " "

#ws-icon-0 = 1;
#ws-icon-1 = 2;
#ws-icon-2 = 3;
#ws-icon-3 = 4;
#ws-icon-4 = 5;
#ws-icon-5 = 6;
#ws-icon-6 = 7;
#ws-icon-7 = 8;
#ws-icon-8 = 9;
#ws-icon-9 = 10;
#ws-icon-default = " "

format = <label-state> <label-mode>
label-mode = %mode%
label-mode-padding = 2
label-mode-foreground = #000000
label-mode-background = #FFBB00

; focused = Active workspace on focused monitor
label-focused = %name%
label-focused-background = ${colors.workspace-active}
label-focused-foreground = ${colors.foreground}
label-focused-underline = #F08080
label-focused-padding = 2

; unfocused = Inactive workspace on any monitor
label-unfocused = %name%
label-unfocused-padding = 2
label-unfocused-background = ${colors.background-i3}
label-unfocused-foreground = ${colors.foreground}
label-unfocused-underline =

; visible = Active workspace on unfocused monitor
label-visible = %name%
label-visible-background = ${self.label-focused-background}
label-visible-underline = ${self.label-focused-underline}
label-visible-padding = 2

; urgent = Workspace with urgency hint set
label-urgent = %name%
label-urgent-background = ${self.label-focused-background}
label-urgent-foreground = #db104e
label-urgent-padding = 2

format-foreground = ${colors.foreground}
format-background = ${colors.background}

#--------------------------------------------------------

[module/wired-network]
;https://github.com/jaagr/polybar/wiki/Module:-network
type = internal/network
interface = enp8s0
;interface = enp14s0
interval = 10

; Available tokens:
;   %ifname%    [wireless+wired]
;   %local_ip%  [wireless+wired]
;   %essid%     [wireless]
;   %signal%    [wireless]
;   %upspeed%   [wireless+wired]
;   %downspeed% [wireless+wired]
;   %linkspeed% [wired]
; Default: %ifname% %local_ip%
label-connected =  %local_ip%
label-disconnected = %ifname% disconnected

format-connected-foreground = ${colors.foreground}
#format-connected-background = ${colors.background}
format-connected-underline = #55aa55
format-connected-prefix = " "
format-connected-prefix-foreground = #aaffc32a
#format-connected-prefix-background = ${colors.background}

format-disconnected = <label-disconnected>
format-disconnected-underline = ${colors.alert}
label-disconnected-foreground = ${colors.foreground}

[module/battery] 
type = custom/script 
exec = $HOME/.bin/polybar-modules.sh battery 
interval = 10 

[module/datetime] 
type = custom/script 
exec = $HOME/.bin/polybar-modules.sh datetime 
interval = 10 

[module/kbdlayout] 
type = custom/script 
exec = $HOME/.bin/polybar-modules.sh kbdlayout 
interval = 10 

[module/network] 
type = custom/script 
exec = $HOME/.bin/polybar-modules.sh network
#format-prefix = ""
interval = 2 

[module/sensor_cpu] 
type = custom/script 
exec = $HOME/.bin/polybar-modules.sh sensor_cpu
#format-prefix = "﨎 "
interval = 2 

[module/sensor_gpu] 
type = custom/script 
exec = $HOME/.bin/polybar-modules.sh sensor_gpu
#format-prefix = "﨎 "
interval = 2 

[module/volume] 
type = custom/script 
exec = $HOME/.bin/polybar-modules.sh volume 
interval = 10 
