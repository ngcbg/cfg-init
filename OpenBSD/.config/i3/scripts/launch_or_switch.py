#!/usr/bin/env python3

import i3ipc
import sys
from time import sleep
import logging

logging.basicConfig(filename='/tmp/launch_or_switch.log', level=logging.DEBUG)
# mapping of workspace names to their applications
WORKSPACE_APPS = {
    'T': 'xfce4-terminal', #fast
    'W': 'firefox', #fast
    'P': 'keepassxc', #slow
    'M': 'audacious', #slow
    'F': 'thunar', #slow
    'A': 'kitty', #fast
    'B': 'lxappearance' #fast
}


def main(workspace, app):

    # Connect to i3
    i3 = i3ipc.Connection()
    # Get the list of workspaces
    workspaces = i3.get_workspaces()
    # Check if workspace exists
    if not any(ws for ws in workspaces if ws.name == workspace):
        i3.command(f'exec {app}')
        sleep(0.5)
        # If not, switch to that workspace and launch the app
        i3.command(f'workspace {workspace}')
    else:
        # If it exists, simply switch to that workspace
        i3.command(f'workspace {workspace}')


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("Usage: launch_or_switch.py <workspace_name>")
        sys.exit(1)

    workspace_name = sys.argv[1]
    app = WORKSPACE_APPS.get(workspace_name, None)

    if not app:
        print(f"No application mapping found for workspace: {workspace_name}")
        sys.exit(1)

    main(workspace_name, app)
