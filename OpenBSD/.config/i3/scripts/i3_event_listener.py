#!/usr/bin/env python3

import i3ipc

i3 = i3ipc.Connection()


def on_window_new(i3, event):
    """
    If the window is an instance of Okular
    (or whatever the correct criteria is)
    """
    if 'alacritty' in event.container.window_class.lower():
        event.container.command('move to workspace T')
    if 'xterm' in event.container.window_class.lower():
        event.container.command('move to workspace T')
    if 'pcmanfm' in event.container.window_class.lower():
        event.container.command('move to workspace F')
    if 'thunar' in event.container.window_class.lower():
        event.container.command('move to workspace F')
    if 'audacious' in event.container.window_class.lower():
        event.container.command('move to workspace M')
    if 'keepassxc' in event.container.window_class.lower():
        event.container.command('move to workspace P')


# Subscribe to the window::new event
i3.on('window::new', on_window_new)

# Start the main loop
i3.main()
