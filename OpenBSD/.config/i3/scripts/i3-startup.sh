#!/usr/local/bin/bash

export DE=xfce
export VM=I3
export COLORTERM=truecolor
export XDG_CURRENT_DESKTOP=xfce

xset b off
xrdb -merge ~/.Xresources &
. ~/.bashrc
setxkbmap -layout us,bg -variant ,phonetic -option grp:alt_shift_toggle

#sed -i "s/.*gtk-theme-name=.*/gtk-theme-name=Arc-Dark/g" ~/.config/gtk-3.0/settings.ini
#sed -i "s/.*gtk-icon-theme-name=.*/gtk-icon-theme-name=Bluecurve8/g" ~/.config/gtk-3.0/settings.ini
#sed -i "s/.*gtk-theme-name=.*/gtk-theme-name=\"Arc-Dark\"/g" ~/.gtkrc-2.0
#sed -i "s/.*gtk-icon-theme-name=.*/gtk-icon-theme-name=\"Bluecurve8\"/g" ~/.gtkrc-2.0

if [ -d /usr/share/fonts ]; then
    for i in /usr/share/fonts/*; do
        xset fp+ $i
    done
    xset fp rehash
fi

if [ -d /home/ngc/.fonts ]; then
    for i in /home/ngc/.fonts/*; do
        xset fp+ $i
    done
    xset fp rehash
fi

notify-send "Welcome back $USER!"
notify-send "$(fortune)"
