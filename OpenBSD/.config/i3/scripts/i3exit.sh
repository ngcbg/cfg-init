#!/bin/sh


case "$1" in
    lock)
        i3lock
        ;;
    logout)
        i3-msg exit
        ;;
    suspend)
        zzz
        ;;
    hibernate)
        ZZZ
        ;;
    reboot)
        doas /sbin/reboot
        ;;
    shutdown)
        doas /sbin/halt -p
        ;;
    *)
        echo "Usage: $0 {lock|logout|suspend|hibernate|reboot|shutdown}"
        exit 2
esac

exit 0
