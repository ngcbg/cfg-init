#!/bin/ksh
kbdlayout=$(xset -q|grep LED| awk '{ print $10 }' | cut -c7)
if [ $kbdlayout = "1" ]
# then print BG
then echo ~/.config/tint2/icons/bg.png
elif [ $kbdlayout = "0" ] 
# then print US
then echo ~/.config/tint2/icons/US.png
fi

