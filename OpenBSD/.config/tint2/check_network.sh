#!/bin/sh

# 
#  Copyright (c) 2018 Vincent Delft <vincent.delft@gmail.com>
# 
#  Permission to use, copy, modify, and distribute this software for any
#  purpose with or without fee is hereby granted, provided that the above
#  copyright notice and this permission notice appear in all copies.
# 
#  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
#  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
#  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
#  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
#  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
#  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
# 

if [ "$(pgrep -f /usr/local/bin/nmctl)" != "" ]; then
    echo ~/.config/tint2/icons/searching.png
    echo "nmctl is running" >&2
    exit 0
fi

#grep interface on your machine
EXT_IFS=$(ifconfig | grep -B1 "laddr" | awk '/^[a-z0-0]+:/ {gsub(":",""); print $1}')

CONNECTED=
WIFI=
CABLE_PLUGED=
for EXT_IF in $EXT_IFS
do
    ifconfig=$(ifconfig "$EXT_IF")
    if [ "$(echo $ifconfig | grep flags | grep UP)" -a "$(echo $ifconfig | grep inet)" -a "$(echo $ifconfig | grep active)" ]; then
        CONNECTED=$EXT_IF
    fi
    if [ "$(echo $ifconfig | grep flags | grep UP)" -a "$(echo $ifconfig | grep Ethernet)" -a "$(echo $ifconfig | grep active)" ]; then
        CABLE_PLUGED=$EXT_IF
    fi
    if [ "$(echo $ifconfig | grep flags | grep UP)" -a "$(echo $ifconfig | grep IEEE)" -a "$(echo $ifconfig | grep inet)"  -a "$(echo $ifconfig | grep active)" ]; then
        WIFI=$EXT_IF
    fi
done

#echo "Connnected:$CONNECTED, cable_pluged: $CABLE_PLUGED, wifi:$WIFI"

#actions
if [ -n "$CABLE_PLUGED" -a "$CABLE_PLUGED" != "$CONNECTED" ]; then
    echo "We could connect via cable"
    nmctl -D1 -L /tmp/nmctl.log cable
    exit
fi
#if [ "$(ifconfig | grep _NM)" ]; then
#    rm /tmp/wifi_trial
#fi


CMD="ifconfig $WIFI"
if [ "$CONNECTED" -a ! "$WIFI" ]; then
   echo ~/.config/tint2/icons/network-wired.png  
   echo "cable connection" >&2
elif [ $WIFI ]; then
    WIFI_VALUE1=$($CMD | grep ieee8 | grep -o -e "...%" -e "...dBm " )
    NWID=$($CMD  | grep ieee | grep -o "nwid .* chan")
    NWID=$NWID$($CMD  | grep ieee | grep -o "join .* chan")
    NWID=${NWID% *}
    NWID=${NWID#* }
    if [ "$1" = "-name" ]; then
        xterm -hold -geometry 65x4 -e "echo $NWID $WIFI_VALUE" &
    else
       #depending on the wifi driver it can be 45%, -66dBm
       WIFI_VALUE=$($CMD | grep ieee8 | grep -o -e "...%" -e "...dBm" | cut -c1-3 )
       if [ "$WIFI_VALUE" ]; then
           WIFI_LEVEL=$(expr $WIFI_VALUE / 25)
           WIFI_LEVEL=${WIFI_LEVEL##-}
           UNIT=""
           if [ "$($CMD | grep dBm)" ]; then
              #in case of dBm icons are inversed.
              UNIT="dBm"
           fi
           echo ~/.config/tint2/icons/network-wireless-connected-$WIFI_LEVEL$UNIT.png 
           echo "$NWID $WIFI_VALUE1" >&2
       else
           echo ~/.config/tint2/icons/network-no-carrier.png 
           echo "no network connection" >&2
       fi
    fi
else
    echo ~/.config/tint2/icons/network-no-carrier.png 
    echo "no network connection" >&2
    # done via ifstated
    #if [ ! -f "/tmp/wifi_trial" ]; then
    #    touch /tmp/wifi_trial
    #    nmctl -r -D1 -L /tmp/nmctl.log
    #fi
fi
