#!/bin/ksh

#
#  Copyright (c) 2017-2020 Vincent Delft <vincent.delf@gmail.com>
#
#  Permission to use, copy, modify, and distribute this software for any
#  purpose with or without fee is hereby granted, provided that the above
#  copyright notice and this permission notice appear in all copies.
#
#  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
#  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
#  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
#  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
#  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
#  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
#
#  26/05/2020 : use of sndioctl instead of mixerctl
#


if [ "i$1" = "i-t" ]; then
   sndioctl -q output.mute=!
   shift
fi
STATUS=$(sndioctl -n output.mute)
if [ "$STATUS" = "1" ]; then
  echo ~/.config/tint2/icons/audio0.png
  echo "mutted" >&2
else
   SOUND_LEVEL=$(sndioctl -n output.level)
   if [ $1 ]; then
      SOUND_LEVEL=$(echo "$SOUND_LEVEL + $1 / 100 " | bc -l)
      if [ $SOUND_LEVEL -lt 0 ]; then
          SOUND_LEVEL=0
      fi
      if [ $SOUND_LEVEL -gt 1 ]; then
          SOUND_LEVEL=1
      fi
      sndioctl -q output.level=$SOUND_LEVEL
   fi
   SOUND_VALUE=$(echo "$SOUND_LEVEL * 10 " | bc | sed 's/^\./0\./')
   [ -z $SOUND_VALUE ] && SOUND_VALUE=0
   SOUND_IMG="${SOUND_VALUE%.*}"
   [ "$SOUND_IMG" -eq "0" -a "${SOUND_LEVEL#*.}" -gt "0" ] && SOUND_IMG=1
   echo ~/.config/tint2/icons/audio${SOUND_IMG}.png
   echo "Sound level: $(printf '%.2f' $SOUND_VALUE)" >&2
fi
