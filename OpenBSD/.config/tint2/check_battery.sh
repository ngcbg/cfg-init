#!/bin/sh

# 
#  Copyright (c) 2017 Vincent Delft <vincent.delf@gmail.com>
# 
#  Permission to use, copy, modify, and distribute this software for any
#  purpose with or without fee is hereby granted, provided that the above
#  copyright notice and this permission notice appear in all copies.
# 
#  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
#  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
#  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
#  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
#  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
#  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
# 

EXIST=$(apm -b)
if [ "i$EXIST" != "i4" -o "i$EXIST" != "i255" ]; then 
    PLUG=$(apm -a)
    CHARGE=$(apm -l)
    LEVEL=$(expr $CHARGE / 20)
    echo ~/.config/tint2/icons/battery-$LEVEL-$PLUG.png
    apm | head -n1 >&2
    apm | head -n1 | awk '{print $4}' | sed 's/\ %//g'>&1
#    FILE=/tmp/.brightness
#  	 CURR_VAL=$(<$FILE)
#    echo "Display Brightness $CURR_VAL" >&2
    if [ "$(apm -m)" != "unknown" ]; then
        if [ "$(apm -m)" -le "15" ]; then
            xterm -hold -geometry 65x4 -e apm &
        fi
    fi
fi
