
configuration {
    modi:                       "drun,run,window";
    show-icons:                 true;
    display-drun:               "";
    display-run:                "";
    display-window:             "";
    drun-display-format:        "{name}";
    window-format:              "{w} · {c} · {t}";
}

/*****----- Global Properties -----*****/
* {
    font:                        "SF Pro Display Medium 14";
//    background:                  #050e14;
    background:                  #2e344095;
    background-alt:              #282A2E80;
    foreground:                  #e5e9f0;
    selected:                    #F07489;
    active:                      #6E77FF;
    urgent:                      #8E3596;
}

/*****----- Main Window -----*****/
window {
    /* properties for window widget */
    transparency:                "real";
    location:                    center;
    anchor:                      center;
    fullscreen:                  false;
    width:                       830px;
    x-offset:                    0px;
    y-offset:                    0px;

    /* properties for all widgets */
    enabled:                     true;
    border-radius:               0px;
    cursor:                      "default";
    background-color:            @background;
}

/*****----- Main Box -----*****/
mainbox {
    enabled:                     true;
    spacing:                     0px;
    background-color:            transparent;
    orientation:                 vertical;
    children:                    [ "inputbar", "listbox" ];
}

listbox {
    spacing:                     20px;
    padding:                     20px;
    background-color:            transparent;
    orientation:                 vertical;
    children:                    [ "message", "listview" ];
}

/*****----- Inputbar -----*****/
inputbar {
    enabled:                     true;
    spacing:                     10px;
    padding:                     80px 30px;
    background-color:            transparent;
/*default url("~/.config/rofi/themes/ib-black-coffin--high-contrast.jpg",width)*/
    background-image:            url("~/Pictures/Wallpaper/art/julian-calle-planet300.jpg", width);
    text-color:                  @foreground;
    orientation:                 horizontal;
    children:                    [ "textbox-prompt-colon", "entry", "dummy", "mode-switcher" ];
}
textbox-prompt-colon {
    enabled:                     true;
    expand:                      false;
    str:                         "";
    padding:                     12px 20px 12px 15px;
    border-radius:               100%;
    background-color:            @background-alt;
    text-color:                  inherit;
}
entry {
    font:                        "SF Pro Display Medium 13";
    enabled:                     true;
    expand:                      false;
    width:                       350px;
    padding:                     12px 16px;
    border-radius:               100%;
    background-color:            @background-alt;
    text-color:                  inherit;
    cursor:                      text;
    placeholder:                 "Search";
    placeholder-color:           inherit;
}
dummy {
    expand:                      true;
    background-color:            transparent;
}

/*****----- Mode Switcher -----*****/
mode-switcher{
    enabled:                     true;
    spacing:                     10px;
    background-color:            transparent;
    text-color:                  @foreground;
}
button {
    width:                       45px;
    padding:                     12px 14px 12px 10px;
    border-radius:               100%;
    background-color:            @background-alt;
    text-color:                  inherit;
    cursor:                      pointer;
}
button selected {
    background-color:            @selected;
    text-color:                  @background;
}

/*****----- Listview -----*****/
listview {
    enabled:                     true;
    columns:                     3;
    lines:                       3;
    cycle:                       true;
    dynamic:                     true;
    scrollbar:                   false;
    layout:                      vertical;
    reverse:                     false;
    fixed-height:                true;
    fixed-columns:               true;

    spacing:                     10px;
    background-color:            transparent;
    text-color:                  @foreground;
    cursor:                      "default";
}

/*****----- Elements -----*****/
element {
    enabled:                     true;
    spacing:                     10px;
    padding:                     4px 20px;
    border-radius:               1%;
    background-color:            transparent;
    text-color:                  @foreground;
    cursor:                      pointer;
    orientation: vertical;
}
element normal.normal {
    background-color:            inherit;
    text-color:                  inherit;
}
element normal.urgent {
    background-color:            @urgent;
    text-color:                  @foreground;
}
element normal.active {
    background-color:            @active;
    text-color:                  @foreground;
}
element selected.normal {
    background-color:            @selected;
    text-color:                  #eceff4;
}
element selected.urgent {
    background-color:            @urgent;
    text-color:                  @foreground;
}
element selected.active {
    background-color:            @urgent;
    text-color:                  @foreground;
}
element-icon {
    background-color:            transparent;
    text-color:                  inherit;
    size:                        5em;
//    size:                        48px;
    cursor:                      inherit;
}
element-text {
    background-color:            transparent;
    text-color:                  inherit;
    cursor:                      inherit;
    vertical-align:              0.5;
    horizontal-align:            0.5;
}

/*****----- Message -----*****/
message {
    background-color:            transparent;
}
textbox {
    padding:                     12px;
    border-radius:               100%;
    background-color:            @background-alt;
    text-color:                  @foreground;
    vertical-align:              0.5;
    horizontal-align:            0.0;
}
error-message {
    padding:                     12px;
    border-radius:               20px;
    background-color:            @background;
    text-color:                  @foreground;
}
