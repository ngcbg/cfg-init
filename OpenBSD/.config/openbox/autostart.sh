# Load X options if any
xrdb -merge ~/.Xresources &
#xrdb -merge /etc/X11/xenodm/Xresources

# Environment
. ~/.profile
. ~/.bashrc

#Application and eyecandy integration
#export DE=gnome

#Enable changing keyboard layouts
setxkbmap -layout us,bg -variant ,phonetic -option grp:alt_shift_toggle

#Small composite manager
#xcompmgr -cCfFD2 &
xcompmgr -c -C -r4.2 -o.55 -f -F -D3 &
#compton &

# Provides desktop functions
#/usr/local/bin/pcmanfm --desktop &
nitrogen --restore

# Start tint2 task&launcher bar
/usr/local/bin/tint2 &

# Start X with numlock on
numlockx &

# Disable bell
xset b off &

# Notifications for OpenBox
dunst &
#/usr/local/lib/xfce4/notifyd/xfce4-notifyd &

# Power manager for notifications
#/usr/local/bin/xfce4-power-manager &

# Set initial brightness level
touch /tmp/.brightness && echo "100" > /tmp/.brightness

# Display temperature color changer
sct 5500 1>/dev/null &
