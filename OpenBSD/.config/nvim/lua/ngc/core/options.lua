vim.cmd("let g:netrw_liststyle = 3")

local opt = vim.opt

opt.relativenumber = true
opt.number = true

-- tabs & indentation
opt.tabstop = 4 -- 4 spaces for tabs
opt.shiftwidth = 4 -- 4 spaces for indent width
opt.expandtab = true -- expand tab to spaces
opt.autoindent = true -- copy indent from current line when starting new one

opt.wrap = false

-- serach settings
opt.ignorecase = true -- ignore case when searching
opt.smartcase = true -- if mixed case in the search, assumes you want a case-sensitive one

opt.cursorline = true

-- turn on termguicolors
opt.termguicolors = true
opt.background = "dark" -- colorschemes that could be light or dark wiil be made dark
opt.signcolumn = "yes" -- show sign column so that text doesn't shift

-- backspace
opt.backspace = "indent,eol,start" -- allow backspace on indent, EOL or insert mode start possition

-- clipboard
opt.clipboard:append("unnamedplus") -- use system clipboard as a default register

-- splitting
opt.splitright = true -- split vertical window to the right
opt.splitbelow = true -- split horizontal window to the bottom

