# NNN
export NNN_PLUG='f:finder;o:fzopen;p:mocq;d:diffs;t:nmount;v:imgview;p:preview-tabbed;P:preview-tui'
export NNN_BMS="c:~/.config;h:~;d:~/Documents;D:~/Downloads;g:~/cfg-init"
export NNN_USE_EDITOR=1
#export NNN_PAGER=bat
export NNN_TERMINAL=xterm-256colors
#export NNN_OPENER=nnnopen
export NNN_COLORS='#0a1b2c3d'
alias nnn "nnn -adHBR"
set --export NNN_FIFO "/tmp/nnn.fifo"
