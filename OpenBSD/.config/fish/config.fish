# vim:nospell:ft=sh:
if status is-interactive
    # Commands to run in interactive sessions can go here
end
set LANG en_US.UTF-8
set LC_ALL en_US.UTF-8
set LC_CTYPE en_US.UTF-8
set LANGUAGE en_US.UTF-8
set -x TODAY (date +"%d-%b-%Y")
set -gx XDG_CACHE_HOME /tmp

set -g -x PATH /usr/bin $PATH
set -g -x fish_greeting ''
set HISTCONTROL ignoreboth:erasedups
set COLORTERM truecolor
set -gx MANPAGER "less -iMFceqwRSX -x2"
set -gx PAGER "less -iMFceqwRSX -x2"
set -gx VISUAL vim
set -gx FCEDIT vim
set -gx EDITOR vim

# make typing ^ actually inserting a "^" and not stderr redirect
set -U fish_features stderr-nocaret qmark-noglob

source /usr/local/share/fish/functions/fzf-key-bindings.fish
#fzf_key_bindings

#Aliases
alias la='ls -a'
alias ll='eza -alh -s name --group-directories-first --octal-permissions --icons'
alias l='ls -l'
alias l.="ls -A | egrep '^\.'"
alias lld='ls -lFahdo'
#alias lsd='ls -d *'

alias bat="bat -n"
alias mc="mc -u"
alias less="less -iMFceqwRSX -x2"
alias sensors='sysctl hw.sensors'
alias cls='echo -ne "\033c"'
alias mkdir='mkdir -p'
alias m="mcedit -x"
alias v="vim"
alias n="nano"
#alias less='less.sh'
alias da='date "+%A, %B %d, %Y [%T]"'
# 
alias camvideo='ffmpeg -f v4l2 -input_format mjpeg -video_size 1280x720 -i /dev/video0 test2.mkv'
#alias camvideo='ffmpeg -f v4l2 -input_format mjpeg -video_size 1280x720 -i /dev/video0 -c:v copy test1.mkv'
# NB # extract the unaltered jpeg files inside the stream $ ffmpeg -i test.mp4 -vcodec copy %03d.jpg
#alias man="man -H"
alias lsof='fstat'
alias eg='egrep -i'
alias grep='grep -i'
alias h='fc -l'
alias portaudit='/usr/sbin/pkg audit -F'
alias svim="doas vim"
alias svi="doas vi"

#fix obvious typo's
alias cd..='cd ..'
alias pdw="pwd"
alias ..='cd ..'
alias ...='cd ...'

#readable output
alias df='df -h'
alias du='du -ch'
alias weather='curl http://wttr.in/Sofia'

#free
alias free="top -d1 | head -n 6"

#continue download
alias wget="wget -c"

#merge new settings
alias merge="xrdb -merge ~/.Xresources"

# Aliases for software managment
alias pkga='doas pkg_add -ivV'
alias pkgq='doas pkg_info -Q'
alias pkgi='doas pkg_info'
alias pkg_check='pkg_libchk'
alias pkg_locate="echo /usr/ports/*/*|tr ' ' '\n'"
alias update='doas pkg_add -u'

#ps
alias psa="ps aux"
alias psgrep="ps aux | grep -v grep | grep -i -e VSZ -e"

#shutdown or reboot
alias dsn="doas shutdown now"
alias dr="doas reboot"

#give the list of all installed desktops - xsessions desktops
alias xd="bat /home/ngc/.xsession"

source ~/.config/fish/nnn.fish

#time neofetch
#time pfetch
ufetch
echo
    da
echo
    cal
