    <!-- ______________________________________ Go to desktop /left-right/ -->
    <keybind key="C-A-Left">
--
    </keybind>
    <keybind key="C-A-Right">
--
    <!-- ______________________________________ Send to Right desktop -->
    <keybind key="C-Right">
--
    <!-- ______________________________________ Send to Left desktop -->
    <keybind key="C-Left">
--
    <!-- ______________________________________ Send to Desktop 1 -->
    <keybind key="W-S-1">
--
    <!-- ______________________________________ Send to Desktop 2 -->
    <keybind key="W-S-2">
--
    <!-- ______________________________________ Send to Desktop 3 -->
	<keybind key="W-S-3">
--
    <!-- ______________________________________ Send to Desktop 4 -->
	<keybind key="W-S-4">
--
    <!-- ______________________________________ Go to desktop -->
    <keybind key="W-F1">
--
    </keybind>
    <keybind key="W-F2">
--
    </keybind>
    <keybind key="W-F3">
--
    </keybind>
    <keybind key="W-F4">
--
    <!-- ______________________________________ Minimize all windows -->
    <keybind key="W-d">
--
    <!-- ______________________________________ Close window -->
    <keybind key="A-F4">
--
    <!-- ______________________________________ Unfocus window -->
    <keybind key="A-Escape">
--
    <!-- ______________________________________ Window menu pop -->
    <keybind key="A-space">
--
    <!-- ______________________________________ Maximize window -->
    <keybind key="A-Return">
--
    <!-- ______________________________________ Minimize window -->
    <keybind key="S-Return">
--
    <!-- Keybindings for window switching -->
    <keybind key="A-Tab">
--
    </keybind>
    <keybind key="A-S-Tab">
--
    </keybind>
    <keybind key="C-A-Tab">
--
    <!-- Keybindings for window switching with the arrow keys -->
    <keybind key="W-S-Right">
--
    </keybind>
    <keybind key="W-S-Left">
--
    </keybind>
    <keybind key="W-S-Up">
--
    </keybind>
    <keybind key="W-S-Down">
--
    <!-- _______________________ Move & maximize window to the North -->
    <keybind key="W-Up">
--
    <!-- _______________________ Move & maximize window to the South -->
    <keybind key="W-Down">
--
    <!-- _______________________ Move & maximize window to the West -->
    <keybind key="W-Left">
--
    <!-- _______________________ Move & maximize window to the East -->
    <keybind key="W-Right">
--
    <!-- _______________________ Center the window on monitor 1 -->
    <keybind key="W-KP_5">
--
    <!-- _______________________ Resize window to 50% display size -->
    <keybind key="W-KP_Divide">
--
    <!-- _______________________ Resize window to 50% display size, move to Up-center corner -->
    <keybind key="W-KP_8">
--
    <!-- _______________________ Resize window to 50% display size, move to center-left -->
    <keybind key="W-KP_6">
--
    <!-- _______________________ Resize window to 50% display size, move to Down-center -->
    <keybind key="W-KP_2">
--
    <!-- _______________________ Resize window to 50% display size, move to Left-center  -->
    <keybind key="W-KP_4">
--
    <!-- _______________________ Resize window to 50% display size, move to Down-right corner -->
    <keybind key="W-KP_3">
--
    <!-- _______________________ Resize window to 50% display size, move to Up-right corner -->
    <keybind key="W-KP_9">
--
    <!-- _______________________ Resize window to 50% display size, move to Up-left corner -->
    <keybind key="W-KP_7">
--
    <!-- _______________________ Resize window to 50% display size, move to Down-left corner -->
    <keybind key="W-KP_1">
--
    <!-- ______________________________ Start Rofi /cli executor-/ -->
    <keybind key="W-r">
--
    <!-- ______________________________ Start XTERM -->
    <keybind key="W-t">
--
    <!-- ______________________________ Start Midnight Commander as root -->
    <keybind key="A-m">
--
    <!-- ______________________________ Show OB Menu / as a right mouse action /-->
    <keybind key="C-m">
--
    <!-- ______________________________ Toggle ON/OFF composite desktop effects -->
    <keybind key="W-n">
--
    <!-- ______________________________ Lock desktop (+Xscreensaver) -->
    <keybind key="W-l">
--
    <!-- ______________________________ Suspend -->
    <keybind key="W-z">
--
    <!-- ______________________________ Sleep -->
    <keybind key="W-s">
--
    <!-- ______________________________ Start PCman File manager -->
    <keybind key="W-e">
--
    <!-- ______________________________ Reconfigure OpenBox -->
    <keybind key="C-A-r">
--
    <!-- ______________________________ Toggle screen brightness -->
    <keybind key="W-b">
--
    <!-- _____________________________ Logout binding -->
    <keybind key="C-W-e">
--
    <!-- _____________________________ Reboot binding -->
    <keybind key="C-W-r">
--
    <!-- _____________________________ Shutdown binding -->
    <keybind key="C-W-s">
