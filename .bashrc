PATH=$HOME/bin:/bin:/sbin:/usr/bin:/usr/sbin:/usr/X11R6/bin:/usr/local/bin:/usr/local/sbin:/usr/games
export PATH HOME TERM
export GTK_OVERLAY_SCROLLING=0
#PATH=/sbin:/usr/sbin:/bin:/usr/bin:/usr/X11R6/bin:/usr/local/sbin:/usr/local/bin
#export PATH
: ${HOME='/home/ngc'}
export HOME
umask 022

#export TERM="wsvt25"
#export TERM="xterm"

if [ $TERM = ttyd0 ]; then
            sudo ppp -direct $TERM
    fi
if [ $TERM = ttyd1 ]; then
            sudo ppp -direct $TERM
    fi

set BBLUE="\[\033[1;34m\]"
set BL ="[\e[0;30;30m\]"
set RD ="[\e[0;31;31m\]"
set GR ="[\e[0;32;32m\]"
set YE ="[\e[0;33;33m\]"
set BLU="[\e[0;34;34m\]"
set MAG="[\e[0;35;35m\]"
set CYA="[\e[0;36;36m\]"

set BRD ="[\e[1;31;31m\]"
set BGR ="[\e[1;32;32m\]"
set BYE ="[\e[1;33;33m\]"
set BBLU="[\e[1;34;34m\]"
set BMAG="[\e[1;35;35m\]"
set BCYA="[\e[1;36;36m\]"
set BWHI="[\e[1;37;37m\]"

#PS1='\[\e[0;1;33;40m\](\l)\u@\h:\w\$\[\e[0m\]'
#PS1='\[\e[0;1;36;40m\]\u@\h:\w\$\[\e[0m\]'
#PS1='\[\e[0;1;36;36m\]\u@\h:\[\e[0;36m\]\w\$\e[0;37m\]'
#alias ls='gls -alh --indicator-style=file-type --color=always'
#PS1='\[\033[1;34m\][\[\e[1;35;35m\]\u\[\033[1;34m\]]\[\e[1;35;35m\]@\[\e[1;36;36m\]\h:\[\e[0;36m\]\w\$\e[0;37m\]'
#PS1='\[\033[1;34m\][\[\e[1;36;36m\]\u\[\033[1;34m\]]\[\e[1;36;36m\]\e[1;36;36m\]@\[\033[1;34m\][\e[1;36;36m\]\h\[\033[1;34m\]]\[\e[1;37;37m\]:\[\e[0;36m\]\w\$\e[0;37m\]'
PS1='\[\e[0;1;33;40m\](\l)\u@\h:\w\$\[\e[0m\]'
PAGER=less
export PAGER
alias less="less -N -m -I"
#alias la="ls -palkT"
#alias dir="la"
#alias ll="ls -alh"
alias ll="colorls -GlahAFpqFT"
alias ls="colorls -GlahAFpqFT"
alias dir="colorls -GlahAFpqFT"
alias mc="mc -a"
if [ -f /usr/local/bin/joe ]; then
    alias q="/usr/local/bin/joe"
fi
if [ -f /usr/local/bin/mc ]; then
    alias mc="/usr/local/bin/mc -c"
    alias q="mc -e"
    alias w="mc -v"
#    PAGER="/usr/local/bin/mc -c -v"
#    export PAGER
fi
if [ "$TERM" == "xterm" ]; then
    bind -f /etc/bash.keys.xterm
else
    bind -f /etc/bash.keys.vt220
fi
if [ "$TERM" == "linux" ]; then
    bind -f /etc/bash.keys
fi

#neofetch

alias config='/usr/local/bin/git --git-dir=/home/ngc/cfg-init/ --work-tree=/home/ngc'
